import { CapacitorConfig } from '@capacitor/cli';
const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'OAS-OTG',
  webDir: 'www',
  server: {
    cleartext: true,
  },
  plugins: {
    SplashScreen: {
      androidScaleType: 'CENTER_CROP',
      splashFullScreen: false,
      splashImmersive: false,
    }
  }
};

export default config;
