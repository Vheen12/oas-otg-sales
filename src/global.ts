export const GlobalVariable = Object.freeze({
    BASE_API_URL: 'https://adamis-api-dev.adamco.com.ph/',
    // BASE_API_URL: 'https://adamis.adamco.com.ph/otg/',
    CLIENT_ID: 'b35c50d8-33ca-4f0b-8885-4d8187cb0261',
    APPKEY: 'kRznbD5UWp3f9GdCpuJunQ8Y83PunkpVs73Rxdex'
});

export const URL_STRING = Object.freeze({
    BASE_URL: 'https://adamis-training.adamco.com.ph/index.php/login',
    // BASE_URL: 'https://adamis.adamco.com.ph/index.php/login',
});

export const DeviceStorage = Object.freeze({
    AppInitialize: 'AppInitialize',
    BranchList: 'BranchList',
    UserInformation: 'User_Information',
    ItineraryList: 'Itinerary_Offline',
    QuotationList: 'quotationList',
    ProspectClientList: 'ProspectClientList',
    ResultCodeList: 'resultCodeList',
    Itinerary_Coordinates: 'Itinerary_Coordinates',
    ProvinceList: 'provinceList',
    CityList: 'cityList',
    BarangayList: 'barangayList',
    ActivityList: 'activityList',
    ClientSourceList: 'clientSourceList',
    AgentList: 'agentList',
    OccupationList: 'occupationList',
    ProductList: 'productList',
    ProductModelList: 'productModelList',
    ProductBrandList: 'productBrandList',
    CorporateAccountList: 'corporateAccountList',
    UserAccess: 'userAccess',
    TransferReceiptList: 'transferReceiptList',
});

export const GEOFENCING_REASONS = [
    { Code: 'Client Visited the branch', Name: 'Client Visited the branch', },
    { Code: 'Client wants to meet at a different place', Name: 'Client wants to meet at a different place', },
    { Code: 'Client is not available, visited another prospect', Name: 'Client is not available, visited another prospect', },
    { Code: 'Others (Please Specify)', Name: 'Others (Please Specify)', },
];