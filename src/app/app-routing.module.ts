import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { authGuard } from './guard/auth.guard';


const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./core/home/home.module').then(m => m.HomePageModule),
    canActivate: [authGuard]
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'app-landing',
    loadChildren: () => import('./features/app-landing/app-landing.module').then(m => m.AppLandingPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./core/login/login.module').then(m => m.LoginPageModule),
  },
  {
    path: 'workschedule',
    loadChildren: () => import('./core/workschedule/workschedule.module').then(m => m.WorkschedulePageModule),
    canActivate: [authGuard]

  },
  {
    path: 'timekeeping',
    loadChildren: () => import('./core/timekeeping/timekeeping.module').then(m => m.TimekeepingPageModule),
    canActivate: [authGuard]
  },
  {
    path: 'worksched-client',
    loadChildren: () => import('./features/worksched-client/worksched-client.module').then(m => m.WorkschedClientPageModule),
    canActivate: [authGuard]
  },
  {
    path: 'timekeeping-time-out',
    loadChildren: () => import('./features/timekeeping-time-out/timekeeping-time-out.module').then(m => m.TimekeepingTImeOutPageModule),
    canActivate: [authGuard]
  },
  {
    path: 'sync-mode',
    loadChildren: () => import('./features/sync-mode/sync-mode.module').then(m => m.SyncModePageModule),
  },
  {
    path: 'prospect-profile-form',
    loadChildren: () => import('./core/prospect-profile-form/prospect-profile-form.module').then(m => m.ProspectProfileFormPageModule),
    canActivate: [authGuard]
  },
  {
    path: 'asq-list',
    loadChildren: () => import('./core/asq-list/asq-list.module').then(m => m.AsqListPageModule),
    canActivate: [authGuard]
  },
  {
    path: 'add-client',
    loadChildren: () => import('./core/add-client/add-client.module').then(m => m.AddClientPageModule),
    canActivate: [authGuard]
  },
  {
    path: 'faqs',
    loadChildren: () => import('./core/faqs/faqs.module').then(m => m.FaqsPageModule)
  },
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule),
    canActivate: [authGuard]
  },
  {
    path: 'user-account',
    loadChildren: () => import('./core/user-account/user-account.module').then(m => m.UserAccountPageModule),
    canActivate: [authGuard]
  },
  {
    path: 'pdf-viewer',
    loadChildren: () => import('./features/pdf-viewer/pdf-viewer.module').then(m => m.PdfViewerPageModule),
    canActivate: [authGuard]
  },
  {
    path: 'rating',
    loadChildren: () => import('./features/rating/rating.module').then(m => m.RatingPageModule),
    canActivate: [authGuard]
  },
  {
    path: 'test',
    loadChildren: () => import('./pages/test/test.module').then( m => m.TestPageModule)
  },  {
    path: 'permission',
    loadChildren: () => import('./features/permission/permission.module').then( m => m.PermissionPageModule)
  },
  {
    path: 'qr-scanner',
    loadChildren: () => import('./features/qr-scanner/qr-scanner.module').then( m => m.QrScannerPageModule)
  },
  {
    path: 'tr-list',
    loadChildren: () => import('./features/tr-list/tr-list.module').then( m => m.TrListPageModule)
  },
  {
    path: 'dr-attachment',
    loadChildren: () => import('./features/dr-attachment/dr-attachment.module').then( m => m.DrAttachmentPageModule)
  },
  {
    path: 'home-bao',
    loadChildren: () => import('./core/home-bao/home-bao.module').then( m => m.HomeBaoPageModule)
  },




];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
