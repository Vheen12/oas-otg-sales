import { Component, OnInit, OnDestroy } from '@angular/core';
import * as CordovaSQLiteDriver from 'localforage-cordovasqlitedriver';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import { register } from 'swiper/element/bundle';

register();
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {

  private subscription = new Subscription();

  constructor(
    private storage: Storage,
  ) { }

  async ngOnInit() {
    await this.storage.defineDriver(CordovaSQLiteDriver);
    await this.storage.create();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}