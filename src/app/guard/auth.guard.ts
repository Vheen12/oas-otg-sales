import { CanActivateFn, Router } from '@angular/router';
import { inject } from '@angular/core';

export const authGuard: CanActivateFn = (route, state) => {

  const isLoggedIn = localStorage.getItem('Authenticated');
  const router = inject(Router);

  if (isLoggedIn === '0') {
    return true;
  } else {
    router.navigate(['/login']);
    return false;
  }
};
