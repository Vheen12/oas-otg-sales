import { Component, OnInit } from '@angular/core';
import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';
import { EntityType } from 'name-entity-recognition';
import Swal from 'sweetalert2';
import Tesseract from 'tesseract.js';
@Component({
  selector: 'app-test',
  templateUrl: './test.page.html',
  styleUrls: ['./test.page.scss'],
})
export class TestPage implements OnInit {

  constructor(
  ) {

  }

  ngOnInit() {
  }

  async test() {
    const worker = await Tesseract.createWorker('eng');
    const { data: { text } } = await worker.recognize(this.selectedImage);
    // const { data: { text } } = await worker.recognize('https://tesseract.projectnaptha.com/img/eng_bw.png');
    console.log(text);
    await worker.terminate();
    Swal.close();
  }


  selectedImage: string;
  imageText: string;


  getPicture() {
    Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Camera
    })
      .then((imageData: any) => {
        this.showLoader();
        this.selectedImage = imageData?.dataUrl;
      })
      .catch((error: Error) => {
        console.log(error?.message);
      })
      .finally(() => this.test());
  }

  selectPicture() {
    Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Photos
    })
      .then((imageData: any) => {
        this.showLoader();
        this.selectedImage = imageData?.dataUrl;
      })
      .catch((error: Error) => {
        console.log(error?.message);
      })
      .finally(() => {
        this.recognizeImage();
        this.test();
      });
  }



  recognizeImage() {

    Tesseract.recognize(this.selectedImage)
      .then((res: any) => {
        Swal.close();
        console.log("Result: ", res);
        // res.data.lines.forEach((row: any) => {
        //   console.log(row?.text);
        // });

        this.imageText = res?.data?.text;
      }).catch((error: Error) => {
        console.log("Error!", error);
      });

  }

  showLoader() {
    Swal.fire({
      title: "Extracting Text from Image...",
      timerProgressBar: true,
      heightAuto: false,
      didOpen: () => {
        Swal.showLoading();
      },
    })
  }


}
