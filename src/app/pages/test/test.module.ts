import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TestPageRoutingModule } from './test-routing.module';

import { TestPage } from './test.page';
import { NgxScannerTextModule } from 'ngx-scanner-text';
import { NameEntityRecognitionModule } from 'name-entity-recognition';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TestPageRoutingModule,
    NgxScannerTextModule,
    NameEntityRecognitionModule,
  ],
  declarations: [TestPage]
})
export class TestPageModule { }
