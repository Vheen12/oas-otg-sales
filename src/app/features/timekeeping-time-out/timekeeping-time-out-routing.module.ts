import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TimekeepingTImeOutPage } from './timekeeping-time-out.page';

const routes: Routes = [
  {
    path: '',
    component: TimekeepingTImeOutPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TimekeepingTImeOutPageRoutingModule {}
