import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TimekeepingTImeOutPageRoutingModule } from './timekeeping-time-out-routing.module';

import { TimekeepingTImeOutPage } from './timekeeping-time-out.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TimekeepingTImeOutPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [TimekeepingTImeOutPage]
})
export class TimekeepingTImeOutPageModule {}
