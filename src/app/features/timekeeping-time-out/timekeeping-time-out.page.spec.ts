import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TimekeepingTImeOutPage } from './timekeeping-time-out.page';
import { IonicStorageModule, Storage } from '@ionic/storage-angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';


describe('TimekeepingTImeOutPage', () => {
  let component: TimekeepingTImeOutPage;
  let fixture: ComponentFixture<TimekeepingTImeOutPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TimekeepingTImeOutPage],
      imports: [
        IonicModule.forRoot(),
        IonicStorageModule,
        HttpClientTestingModule
      ],
      providers: [Storage],
    }).compileComponents();

    fixture = TestBed.createComponent(TimekeepingTImeOutPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
