import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage-angular';
import { Device } from '@capacitor/device';
import { App } from '@capacitor/app';
import { Router } from '@angular/router';
import { GeolocationService } from 'src/app/services/geolocation/geolocation.service';
import { PhotoService } from 'src/app/services/photo/photo.service';
import { Subscription } from 'rxjs';
import Swal from 'sweetalert2';
import { DeviceStorage } from 'src/global';
import * as htmlToImage from 'html-to-image';
import { TimekeepingService } from 'src/app/services/Timekeeping/timekeeping.service';

@Component({
  selector: 'app-timekeeping-time-out',
  templateUrl: './timekeeping-time-out.page.html',
  styleUrls: ['./timekeeping-time-out.page.scss'],
})
export class TimekeepingTImeOutPage implements OnInit {

  private subscription = new Subscription();

  protected attendanceForm: FormGroup;
  protected userData: any;

  branchCoordinates: any;
  dateToday = new Date();

  imageGeoTag: any;
  imageDateTimeTag: any;

  //Button text
  attendanceText: string;

  odometerPicture: any = null;
  LocationAddress: string = null;
  EmployeeNo: string;

  latitudeInput: any;
  longitudeInput: any;

  status: boolean = false;
  geofencingStatus: string = "";

  regex: any = /data:.*base64,/;

  constructor(private storage: Storage,
    private timekeepingService: TimekeepingService,
    private geolocation: GeolocationService,
    public photoservice: PhotoService,
    private router: Router,
    private formBuilder: FormBuilder,
  ) {
    this.attendanceForm = this.formBuilder.group({
      userId: [''],
      employeeNo: [''],
      checkInOut: [''],
      latitude: [''],
      longitude: [''],
      location: [''],
      regID: [''],
      androidID: [''],
      odometer: ['', Validators.required],
      attachment: ['', Validators.required],
      workSetting: [''],
      version: [''],
    });
  }

  ngOnInit() { }

  ionViewWillEnter() {
    this.getUserInfo();
  }

  ionViewWillLeave() {
    this.clearImage();
    this.subscription.unsubscribe();
  }

  async getUserBranch(incomingData: any) {
    this.branchCoordinates = {
      latitude: +incomingData?.baseLat,
      longitude: +incomingData?.baseLng,
    }
  }

  async getUserInfo() {
    await this.storage.get(DeviceStorage.UserInformation)
      .then((res: any) => {

        this.userData = res;
        this.getUserBranch(res);
        return res;

      }).then((res: any) => {
        this.checkUserAttendance(res);
      });
  }


  private async setAttendanceInformation() {
    this.attendanceForm.get('userId')?.setValue(this.userData?.userId);
    this.attendanceForm.get('employeeNo')?.setValue(this.userData?.userId);
    this.attendanceForm.get('checkInOut')?.setValue(((!this.attendanceText) ? 'IN' : this.attendanceText));
    this.attendanceForm.get('latitude')?.setValue(this.latitudeInput);
    this.attendanceForm.get('longitude')?.setValue(this.longitudeInput);
    this.attendanceForm.get('location')?.setValue(this.LocationAddress);
    this.attendanceForm.get('regID')?.setValue((await Device.getId()).identifier);
    this.attendanceForm.get('androidID')?.setValue((await Device.getId()).identifier);
    this.attendanceForm.get('attachment')?.setValue(this.odometerPicture);
    this.attendanceForm.get('workSetting')?.setValue(this.userData?.workSetting);
    this.attendanceForm.get('version')?.setValue((await App.getInfo()).version + '.' + (await App.getInfo()).build);

    if (this.attendanceForm.get('checkInOut').value === 'OUT') {
      this.storage.remove(DeviceStorage.Itinerary_Coordinates);
    }

    this.insertAttendanceInformation(this.attendanceForm.value);
  }

  private insertAttendanceInformation(data: any) {
    const INSERT_ATTENDANCE = this.timekeepingService.insertAttendance(data, this.userData?.token).subscribe({
      next: (res: any) => {

        if (res.statusCode !== "2000") {
          this.presentAlertError(res.message)
          return;
        }

        this.presentAlertSuccess("Your Attendance has been Saved!");

      },
      error: (error: any) => {
        this.presentAlertError(error);
      },
    });

    this.subscription.add(INSERT_ATTENDANCE);
  }


  async capturePhoto() {
    const imageData = await this.photoservice.addNewToGallery();
    this.attendanceForm.get('attachment')?.setValue(imageData);
    this.resetVariables();
    this.getUserCoordinates();
  }


  async clearImage() {
    this.photoservice.photos.splice(0, 1);
    this.resetVariables();
  }


  async getUserCoordinates() {
    await this.geolocation.getCoordinates()
      .then((res: any) => {

        let userLocation = {
          latitude: res.coords.latitude,
          longitude: res.coords.longitude,
        };

        let branchLocation = {
          latitude: this.branchCoordinates?.latitude,
          longitude: this.branchCoordinates?.longitude,
        };

        console.log(userLocation);
        console.log(this.branchCoordinates);

        this.getUserLocation(res.coords.latitude, res.coords.longitude);
        this.getGeofencingStatus(userLocation, branchLocation);
      });
  }


  async getUserLocation(latitude: number, longitude: number) {
    this.geolocation.getLocation(latitude, longitude).then((location) => {

      this.longitudeInput = longitude;
      this.latitudeInput = latitude;
      this.imageDateTimeTag = new Date();
      this.imageGeoTag = `Latitude: ${latitude}  Longitude: ${longitude}`;
      this.LocationAddress = location;
    });
  }


  private getGeofencingStatus(userLocation: any, branchLocation: any) {
    this.geolocation.geofencing(userLocation, branchLocation).then((res: any) => {
      this.geofencingStatus = res.geofence;
      this.status = res.status;
    });
  }


  async setGeotagImage() {
    await htmlToImage.toJpeg(document.getElementById('geotagged-img'))
      .then((dataUrl: string) => {
        this.odometerPicture = dataUrl.replace(this.regex, '');;
      }).finally(() => {
        this.presentLoader();
        this.setAttendanceInformation();
      });
  }

  async checkUserAttendance(data: any) {
    this.timekeepingService
      .getAttendanceToday(data.userId, data.token)
      .subscribe({
        next: (res: any) => {
          if (res.statusCode !== '2000') {
            return;
          }
          this.attendanceText = "OUT";
        },
        error: (error: any) => {
          this.timekeepingService
            .presentAlertError("Timekeeping: Checking Attendance Error", error.message);
        },
        complete: () => { }
      })
  }


  async resetVariables() {
    this.imageDateTimeTag = "";
    this.imageGeoTag = "";
    this.LocationAddress = "";
    this.geofencingStatus = "";
  }


  async presentLoader() {
    Swal.fire({
      title: 'Saving!',
      text: 'Wait for a few seconds!',
      heightAuto: false,
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      }
    });

  }

  async presentAlertError(ErrorMsg: string) {
    Swal.fire({
      title: 'Oops!',
      text: ErrorMsg,
      icon: 'error',
      heightAuto: false,
      allowOutsideClick: false
    });
  }

  async presentAlertSuccess(message: any) {
    Swal.fire({
      title: 'Success!',
      text: message,
      icon: 'success',
      heightAuto: false,
      allowOutsideClick: false
    }).then((result) => {
      if (result.isConfirmed) {
        this.attendanceForm.reset();
        this.odometerPicture = null;
        this.clearImage();
        this.router.navigateByUrl('/timekeeping');
      }
    });
  }

  async presentSubmitPrompt() {
    Swal.fire({
      title: 'Timekeeping',
      icon: 'question',
      text: 'Submit your attendance?',
      heightAuto: false,
      allowOutsideClick: false,
      showCancelButton: true,
      confirmButtonText: 'Submit',
      confirmButtonColor: '#0e8140',
    }).then((result) => {
      if (result.isConfirmed) {

        this.setGeotagImage();
      }
    });
  }
}
