import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pdf-viewer',
  templateUrl: './pdf-viewer.page.html',
  styleUrls: ['./pdf-viewer.page.scss'],
})
export class PdfViewerPage implements OnInit {

  protected PDF_FILE: string = "";
  zoomValue: number = 1;

  constructor() { }

  ngOnInit() {
    let pdf: string = localStorage.getItem("PDF_FILE");
    this.PDF_FILE = "../../../assets/pdf/ADAMCO OAS OTG V2B1.pdf";
  }


  zoomPage(operation: string) {
    if (operation === "add") {
      this.zoomValue += 0.20;
      return;
    }

    if (this.zoomValue > 1) {
      this.zoomValue -= 0.20
    }
  }

}
