import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AppLandingPage } from './app-landing.page';
import { IonicStorageModule, Storage } from '@ionic/storage-angular';

describe('AppLandingPage', () => {
  let component: AppLandingPage;
  let fixture: ComponentFixture<AppLandingPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [AppLandingPage],
      imports: [IonicStorageModule.forRoot()],
      providers: [Storage]
    }).compileComponents();

    fixture = TestBed.createComponent(AppLandingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
