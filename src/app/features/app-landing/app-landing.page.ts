import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { DeviceStorage } from 'src/global';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-app-landing',
  templateUrl: './app-landing.page.html',
  styleUrls: ['./app-landing.page.scss'],
})
export class AppLandingPage implements OnInit {

  public users: any = [
    { position: "Salesman", access: "FMR", image: "/assets/users/salesman.png" },
    { position: "Branch Admin Officer", access: "BAO", image: "../../../assets/users/admin-officer.png" },
    { position: "Branch Manager", access: "BM", image: "../../../assets/users/branch-manager.png" },
    { position: "Sales Manager", access: "SM", image: "../../../assets/users/sales-manager.png" },
  ];

  public userLabel: any;

  constructor(
    private storage: Storage,
    private router: Router,
  ) { }

  ngOnInit() {
    this.storage.get(DeviceStorage.UserAccess)
      .then((res: any) => {

        if (res) {
          this.router.navigate(["/login"]);
        }
      })
  }

  selectedUser(user: any) {
    this.userLabel = user;
  }

  saveUserAccess() {
    Swal.fire({
      title: 'User Access',
      text: `Save access as ${this.userLabel?.position}?`,
      icon: 'question',
      heightAuto: false,
      allowOutsideClick: false,
      showCancelButton: true,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Save',
    }).then((result) => {
      if (result.isConfirmed) {
        this.presentSuccessAlert();
      }
    });
  }

  presentSuccessAlert() {
    Swal.fire({
      title: 'Success!',
      text: 'User Access has been Saved!',
      icon: 'success',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Proceed to Login Page',
    }).then((result) => {
      if (result.isConfirmed) {
        this.storage.set(DeviceStorage.UserAccess, this.userLabel?.access)
          .finally(() => this.router.navigate(['/login']));
      }
    });
  }
}
