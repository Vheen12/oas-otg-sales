import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WorkschedClientPage } from './worksched-client.page';

const routes: Routes = [
  {
    path: '',
    component: WorkschedClientPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorkschedClientPageRoutingModule {}
