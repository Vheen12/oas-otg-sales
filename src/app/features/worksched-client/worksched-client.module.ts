import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WorkschedClientPageRoutingModule } from './worksched-client-routing.module';

import { WorkschedClientPage } from './worksched-client.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WorkschedClientPageRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [WorkschedClientPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WorkschedClientPageModule { }
