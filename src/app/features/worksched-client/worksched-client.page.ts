import { Component, OnInit, OnDestroy } from '@angular/core';
import { SafeResourceUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { WorkscheduleService } from 'src/app/services/workschedule/workschedule.service';
import Swal from 'sweetalert2';
import { PhotoService } from 'src/app/services/photo/photo.service';
import { GeolocationService } from 'src/app/services/geolocation/geolocation.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as htmlToImage from 'html-to-image';
import { DeviceStorage, GEOFENCING_REASONS } from 'src/global';
import { Filesystem, Directory } from '@capacitor/filesystem';
import { NetworkConnectionService } from 'src/app/services/network-connection/network-connection.service';

export interface Coordinates {
  latitude?: number;
  longitude?: number;
  location?: string;
  timestamp?: Date;
}

export interface Geotag {
  latitude: number;
  longitude: number;
  location: string;
  time: Date;
  result?: string;
  remarks?: string;
}
@Component({
  selector: 'app-worksched-client',
  templateUrl: './worksched-client.page.html',
  styleUrls: ['./worksched-client.page.scss'],
})
export class WorkschedClientPage implements OnInit, OnDestroy {

  resultForm: FormGroup;

  test: any;
  userData: any;
  userCoordinates: Coordinates;
  clientCoordinates: Coordinates;
  geoTagging: Geotag;
  reasons = GEOFENCING_REASONS;
  distance: number;
  geofenceStatus: any;
  clientInfo: any;
  clientName: string = "";
  clientAddress: string = "";
  LocationAddress: any;
  networkStatus: any;

  imageReview: SafeResourceUrl;
  imagePreview: SafeResourceUrl;
  regex = /data:.*base64,/;

  locationStatus: boolean = true;
  showReasonInput: boolean = false;

  public resultCodeList: any[] = [];
  timestamp: Date;

  constructor(
    private router: Router,
    public storage: Storage,
    private workschedService: WorkscheduleService,
    public photoservice: PhotoService,
    public geolocation: GeolocationService,
    public formbuilder: FormBuilder,
    private network: NetworkConnectionService,
  ) {
    this.resultForm = this.formbuilder.group({
      CSL_Result: ['', Validators.required],
      CSL_Status_To_Remarks: ['', Validators.required],
      Attachment: ['', Validators.required],
      CSL_Result_Date: [''],
      Latitude: [''],
      Longitude: [''],
      Location: [''],
      CSL_Result_Remarks: [''],
      CLI_Code: [''],
      CSL_Ref_No: [''],
      GeofenceClientStatus: [''],
      GeofenceReason: ['', Validators.required],
    });
  }


  ngOnInit() {
    this.getClientInfo();
  }

  ngOnDestroy() {
    localStorage.removeItem('client_info');
    this.clearImage();
  }

  ionViewWillEnter() {
    this.checkNetworkStatus();
    this.getClientCoordinates();
  }

  ionViewDidLeave() {
    this.clearImage();
  }

  checkNetworkStatus() {
    this.network.checkNetworkConnection()
      .then((res: boolean) => {
        this.networkStatus = res;
      }).finally(() => this.getUserInfo());
  }


  getUserInfo() {
    this.storage.get(DeviceStorage.UserInformation)
      .then((res: any) => {

        this.userData = res;

      }).finally(() => this.getResults());
  }

  getClientInfo() {
    this.clientInfo = JSON.parse(localStorage.getItem('client_info'));

    this.clientName = this.clientInfo.CLI_Lname + ' ' + this.clientInfo.CLI_Fname + ' ' + this.clientInfo.CLI_MName;
    this.clientAddress = this.clientInfo.brgy_desc + ', ' + this.clientInfo.city_desc + ', ' + this.clientInfo.
      province_desc;
  }

  // TODO: FETCH AND STORE RESULTS FROM THE STORAGE

  getResults() {
    this.storage.get(DeviceStorage.ResultCodeList)
      .then((res: any) => {

        this.verifyResults(res);

      })
      .catch((error: any) => {
        this.workschedService
          .presentAlertError("Itinerary: Getting Results Error", error.message);
      });
  }


  verifyResults(res: any) {
    res ? this.setResults(res)
      : this.workschedService
        .presentAlertError("Empty Result List!", "Please Connect to the internet and click Refresh to Fetch data");
  }


  setResults(res: any) {
    this.resultCodeList = (res?.data.filter((x: any) => {
      return x?.Activity === this.clientInfo?.CSL_Status_To;
    }));
  }


  // TODO: FETCH AND STORE CLIENT COORDINATES FROM THE STORAGE

  getClientCoordinates() {
    this.storage.get(DeviceStorage.Itinerary_Coordinates)
      .then((res: any) => {

        this.verifyClientCoordinates(res);

      })
      .catch((error: any) => {
        this.workschedService.presentAlertError("Itinerary: Checking Client Coordinate Error", error.message);
      });
  }


  verifyClientCoordinates(res: any) {
    res ? this.setClientCoordinates(res)
      : this.workschedService
        .presentAlertError("Itinerary: Checking Client Coordinate", res.message);
  }


  setClientCoordinates(res: any) {
    const COORDS = res.filter((data: any) => {
      return data?.CSL_Ref_No === this.clientInfo?.CSL_Ref_No;
    });

    this.clientCoordinates = {
      latitude: COORDS[0]?.CLI_Latitude,
      longitude: COORDS[0]?.CLI_Longitude,
    };
  }


  // TODO: COMPUTE GEOFENCING DISTANCE AND STATUS
  computeDistance() {

    this.userCoordinates = {};
    this.timestamp = null;

    this.geolocation.getCoordinates()
      .then((res: any) => {

        this.timestamp = new Date(res?.timestamp);

        this.userCoordinates = {
          latitude: res.coords.latitude,
          longitude: res.coords.longitude,
        }

        this.getUserLocation(res);

      }).catch((error: any) => {
        this.workschedService
          .presentAlertError('Error Computing Geolocation!', error.message);
      }).finally(() => {
        this.getGeofencingStatus();
      });
  }

  private getUserLocation(res: any) {
    this.geolocation
      .getLocation(res.coords.latitude, res.coords.longitude)
      .then((res: any) => {
        this.userCoordinates = {
          location: res,
        };
      });
  }

  getGeofencingStatus() {
    this.geolocation.geofencing(this.userCoordinates, this.clientCoordinates)
      .then((res: any) => {
        this.geofenceStatus = res?.geofence;
        this.distance = res?.distance;
        this.locationStatus = res?.status;

        this.locationStatus
          && this.resultForm.get('GeofenceReason')?.setValue("Within Area of Client");


      })
      .catch((error: any) => {
        this.workschedService.presentAlertError("Geofencing Calculation", error.message);
      });
  }



  async validateItineraryData(data: any) {
    const RESULT_DATA = [];
    this.getNewImage();
    this.setFormValue(data);


    Swal.fire({
      title: 'Saving Itinerary!',
      heightAuto: false,
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      }
    }).then((result) => {
      if (result.dismiss) {
        RESULT_DATA.push(this.resultForm.value);

        this.networkStatus
          ? this.insertItineraryDataOnline(RESULT_DATA)
          : this.insertItineraryDataOffline(RESULT_DATA, 1);
      }
    });
  }


  async insertItineraryDataOnline(data: any) {
    this.workschedService.updateItinerary(data, this.userData?.token).subscribe({
      next: (res: any) => {
        if (res.statusCode !== '2000') {
          this.workschedService.presentAlertError("Itinerary: Saving Online Status: " + res.statusCode, res.message);
          return;
        }
        this.insertItineraryDataOffline(data, 0)
      },
      error: (error: any) => {
        this.workschedService.presentAlertError("Itinerary: Saving Online", error.message)
      },
      complete: () => { }
    });
  }


  async insertItineraryDataOffline(data: any, status: number) {

    const newData = Object.assign({}, data[0]);

    this.storage.get(DeviceStorage.ItineraryList).then((itinerary) => {
      for (let i = 0; i < itinerary.data.length; i++) {
        const element = itinerary.data[i];
        if (element.CSL_Ref_No === newData.CSL_Ref_No) {
          this.setNewItineraryData(element, newData, status);
        }
        this.storage.set(DeviceStorage.ItineraryList, itinerary);
        this.presentAlertSuccess();
      }
    }).catch((error) => {
      this.workschedService.presentAlertError("Itinerary: Insert Offline Error", error.message);
    });
  }


  async CapturePhoto() {
    const imageData = await this.photoservice.addNewToGallery();
    this.resultForm.get('Attachment').setValue(imageData);

    this.geolocation.getCoordinates()
      .then((res: any) => {

        return res;

      }).then((res: any) => {

        this.setGeotagging(res);

      });
  }


  setGeotagging(data: any) {
    this.geolocation
      .getLocation(data.coords.latitude, data.coords.longitude)
      .then((res: any) => {

        this.geoTagging = {
          time: new Date(data?.timestamp),
          latitude: data?.coords.latitude,
          longitude: data?.coords.longitude,
          location: res,
          result: (this.resultForm.get('CSL_Result_Remarks').value),
          remarks: (this.resultForm.get('CSL_Status_To_Remarks').value),
        };
      });
  }


  async clearImage() {
    this.photoservice.photos.splice(0, 1);
    this.resultForm.get('Attachment')?.setValue('');
    this.LocationAddress = '';
  }


  async getResultRemarks(event: any) {
    const resultremarks = event.detail.value;

    const results = this.resultCodeList.filter((data: any) => {
      return data?.Code === resultremarks;
    });

    this.resultForm.get('CSL_Result').setValue(resultremarks);
    this.resultForm.get('CSL_Result_Remarks').setValue(results[0].Name);
  }


  setFormValue(data: any) {
    this.resultForm.get('Latitude').setValue(data.Latitude);
    this.resultForm.get('Longitude').setValue(data.Longitude);
    this.resultForm.get('Location').setValue(data.Location);
    this.resultForm.get('CSL_Result_Date').setValue(data.CSL_Result_Date);
    this.resultForm.get('GeofenceClientStatus')?.setValue(this.geofenceStatus);
    this.resultForm.get('CLI_Code').setValue((this.clientInfo.CLI_Code !== null ? this.clientInfo.CLI_Code : this.userData?.branchCode));
    this.resultForm.get('CSL_Ref_No').setValue(this.clientInfo.CSL_Ref_No);
  }


  setNewItineraryData(element: any, newData: any, status: number) {
    element.Status = 'Completed';
    element.CSL_Result = newData.CSL_Result;
    element.CSL_Result_Date = newData.CSL_Result_Date;
    element.CSL_Result_Remarks = newData.CSL_Result_Remarks;
    element.CSL_Status_To_Remarks = newData.CSL_Status_To_Remarks;
    element.CSL_Latitude_Time_Out = newData.Latitude;
    element.CSL_Longitude_Time_Out = newData.Longitude;
    element.CSL_Location_Time_Out = newData.Location;
    element.Latitude = newData.Latitude;
    element.Longitude = newData.Longitude;
    element.Location = newData.Location;
    element.CSL_Time_Out_Date = newData.CSL_Result_Date;
    element.completed_Sync_status = status;
    element.Attachment = newData.Attachment;
    element.Distance = this.distance;
    element.GeofenceStatus = this.geofenceStatus;
    element.GeofenceReason = newData.GeofenceReason;
  }


  async presentAlertSuccess() {
    Swal.fire({
      title: 'Success!',
      text: 'Updated Itinerary of Client!',
      icon: 'success',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Continue',
    }).then((result) => {
      if (result.isConfirmed) {
        this.photoservice.photos.splice(0, 1);
        this.router.navigateByUrl('tabs/workschedule');
      }
    });

  }


  async getNewImage() {
    await htmlToImage.toJpeg(document.getElementById('geotagged-img'))
      .then((dataUrl: any) => {

        const newImg: any = dataUrl.replace(this.regex, '');
        const fileName = `result_${this.clientInfo.CLI_Code}_${new Date().getTime()}.jpg`;

        Filesystem.writeFile({
          path: fileName,
          data: newImg,
          directory: Directory.Documents,
        });

        this.resultForm.get('Attachment').setValue(newImg);
      });
  }

  async presentSubmitPrompt() {
    Swal.fire({
      title: 'Complete Itinerary?',
      icon: 'question',
      heightAuto: false,
      allowOutsideClick: false,
      showCancelButton: true,
      confirmButtonText: 'Submit',
      confirmButtonColor: '#0e8140',
    }).then((result) => {
      if (result.isConfirmed) {

        const locationData = {
          CSL_Result_Date: this.geoTagging?.time,
          Latitude: this.geoTagging?.latitude,
          Longitude: this.geoTagging?.longitude,
          Location: this.geoTagging?.location,
        };

        this.validateItineraryData(locationData);

      }
    });
  }


  async showInputField(event: any) {
    const reason = event.detail.value;
    if (reason === 'Others (Please Specify)') {
      this.showReasonInput = true;
      this.resultForm.get('GeofenceReason')?.setValue('');
    } else {
      this.showReasonInput = false;
      this.resultForm.get('GeofenceReason')?.setValue(reason);
    }
  }
}
