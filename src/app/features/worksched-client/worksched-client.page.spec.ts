import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { WorkschedClientPage } from './worksched-client.page';
import { Storage } from '@ionic/storage-angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('WorkschedClientPage', () => {
  let component: WorkschedClientPage;
  let fixture: ComponentFixture<WorkschedClientPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkschedClientPage ],
      imports: [
        IonicModule.forRoot(),
        HttpClientTestingModule
      ],
      providers:[Storage]
    }).compileComponents();

    fixture = TestBed.createComponent(WorkschedClientPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
