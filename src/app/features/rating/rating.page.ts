import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-rating',
  templateUrl: './rating.page.html',
  styleUrls: ['./rating.page.scss'],
})
export class RatingPage implements OnInit {

  rating: number = 0;
  inactiveStar: string = "../../../assets/rating/star-outline.png";
  activeStar: string = "../../../assets/rating/star.png";

  constructor(
  ) { }

  ngOnInit() {
  }

  test(value: number) {
    this.rating = value;
  }

  submitRating() {
    // this.alerts.presentAlertSuccess("Rating has been submitted! Thank you for your response!");
  }

}
