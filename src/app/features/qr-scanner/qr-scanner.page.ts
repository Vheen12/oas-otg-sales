import { Component, OnInit } from '@angular/core';
import { ScannerQRCodeConfig } from 'ngx-scanner-qrcode';
import { Storage } from '@ionic/storage';
import { DeviceStorage } from 'src/global';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-qr-scanner',
  templateUrl: './qr-scanner.page.html',
  styleUrls: ['./qr-scanner.page.scss'],
})
export class QrScannerPage implements OnInit {


  ctr: number = 0;
  tranferReceiptData: any;
  token: string = "";
  serialInput: string = "";
  isModalOpen: boolean = false;

  public QR_VALUE: any;

  public config: ScannerQRCodeConfig = {
    constraints: {
      video: {
        width: window.innerWidth,
        facingMode: { exact: "environment" },
      },
    }
  };


  public canvasstyles = [
    { lineWidth: 1, strokeStyle: 'green', fillStyle: '#55f02880' }
  ];

  constructor(
    private storage: Storage,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getTransferReceipt();
    this.getUserInfo();
  }


  getUserInfo() {
    this.storage.get(DeviceStorage.UserInformation)
      .then((res: any) => {
        this.token = res.token;
      });
  }

  getTransferReceipt() {
    let rawData: any = localStorage.getItem("transfer_receipt");
    this.tranferReceiptData = JSON.parse(rawData);
  }

  doAction(action: any) {
    if (action.isStart) {
      this.ctr = 0;
      this.QR_VALUE = "";
      action.stop();
      return;
    }

    action.start();
  }

  getQRValue(QR_Code: any, action: any) {

    this.QR_VALUE = QR_Code?.[0]?.value;
    let typeName: string = QR_Code?.[0]?.typeName;

    action.pause();

    if (this.QR_VALUE) {
      this.verifySerialNumber(this.QR_VALUE, action, typeName);
      return;
    }

    this.presentNoSerialNumber(action);
  }


  verifySerialNumber(serial_num: string, action: any, typeName: string) {

    if (typeName === "ZBAR_QRCODE") {
      let textIndex = serial_num.search("Serial");
      let serialNo = serial_num.substring(textIndex + 13);

      if (serialNo === '' || !serialNo) {
        this.presentNoSerialNumber(action);
        return;
      }

      if (this.tranferReceiptData.Serial_Number === (serialNo).trim()) {
        this.presentSuccessMatch(action);
        return;
      }

      this.presentInvalidQRCode(action);
    }

    else {

      if (serial_num === '' || !serial_num) {
        this.presentNoSerialNumber(action);
        return;
      }

      if (this.tranferReceiptData.Serial_Number === (serial_num).trim()) {
        this.presentSuccessMatch(action);
        return;
      }

      this.presentInvalidQRCode(action);
    }

  }


  openModal() {
    this.isModalOpen = true;
  }

  closeModal() {
    this.isModalOpen = false;
  }

  verifyBarcode() {
    

    if (this.tranferReceiptData.Serial_Number === (this.serialInput).trim()) {
      this.closeModal();
      this.presentSuccessMatchSerial();
      return;
    }

    this.presentSerialNotMatch();
  }

  presentSuccessMatch(action: any) {
    Swal.fire({
      title: "Success!",
      text: "Serial Number has been verified!",
      icon: "success",
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Proceed to DR Attachment',
    }).then((res) => {
      if (res.isConfirmed) {
        action.play();
        action.stop();
        this.router.navigateByUrl("dr-attachment");
      }
    });
  }

  presentSuccessMatchSerial() {
    Swal.fire({
      title: "Success!",
      text: "Serial Number has been verified!",
      icon: "success",
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Proceed to DR Attachment',
    }).then((res) => {
      if (res.isConfirmed) {
        this.router.navigateByUrl("dr-attachment");
      }
    });
  }


  presentInvalidQRCode(action: any) {
    Swal.fire({
      title: "Oops!",
      text: "Serial Number does not Match! Please Try Again!",
      icon: "error",
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Input Serial',
      showCancelButton: true,
      cancelButtonText: "Scan Again",
    }).then((res) => {
      if (res.isConfirmed) {
        action.stop();
        this.openModal();
      }

      if (res.isDismissed) {
        action.play();
      }
    });
  }

  presentSerialNotMatch() {
    Swal.fire({
      title: "Oops!",
      text: "Serial Number does not Match! Please Try Again!",
      icon: "error",
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Retry',
    });
  }

  presentNoSerialNumber(action: any) {
    Swal.fire({
      title: "Oops!",
      text: "Serial Number is empty!",
      icon: "error",
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Retry',
    }).then((res) => {
      if (res.isConfirmed) {
        action.play();
      }
    });
  }

}
