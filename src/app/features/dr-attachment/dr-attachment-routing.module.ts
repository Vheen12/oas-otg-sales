import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DrAttachmentPage } from './dr-attachment.page';

const routes: Routes = [
  {
    path: '',
    component: DrAttachmentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DrAttachmentPageRoutingModule {}
