import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DrAttachmentPageRoutingModule } from './dr-attachment-routing.module';

import { DrAttachmentPage } from './dr-attachment.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DrAttachmentPageRoutingModule
  ],
  declarations: [DrAttachmentPage]
})
export class DrAttachmentPageModule {}
