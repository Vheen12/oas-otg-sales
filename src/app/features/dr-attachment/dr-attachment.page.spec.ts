import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { DrAttachmentPage } from './dr-attachment.page';

describe('DrAttachmentPage', () => {
  let component: DrAttachmentPage;
  let fixture: ComponentFixture<DrAttachmentPage>;

  beforeEach(waitForAsync () => {
    fixture = TestBed.createComponent(DrAttachmentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
