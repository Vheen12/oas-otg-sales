import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { DeviceStorage } from 'src/global';
import { PhotoService } from 'src/app/services/photo/photo.service';
import { TrListService } from 'src/app/services/tr-list/tr-list.service';
import Swal from 'sweetalert2';
import { Browser } from '@capacitor/browser';

@Component({
  selector: 'app-dr-attachment',
  templateUrl: './dr-attachment.page.html',
  styleUrls: ['./dr-attachment.page.scss'],
})
export class DrAttachmentPage implements OnInit, OnDestroy {


  token: string = "";
  receiptForm: FormGroup;
  transferNumber: void;

  constructor(
    public photoservice: PhotoService,
    private router: Router,
    private storage: Storage,
    private formBuilder: FormBuilder,
    private trListservice: TrListService,
  ) {
    this.receiptForm = this.formBuilder.group({
      RefNo: [""],
      TransferNumber: [""],
      DR_Attachment: ["", Validators.required],
    });
  }

  ngOnInit() {
    this.getUserInfo();
    this.getStackTransferReceipt();
  }

  ngOnDestroy(): void {
    this.clearImage();
  }

  getUserInfo() {
    this.storage.get(DeviceStorage.UserInformation)
      .then((res: any) => {
        this.token = res.token;
      });
  }

  getStackTransferReceipt() {
    let rawData: any = localStorage.getItem("transfer_receipt");
    let parseData: any = JSON.parse(rawData);

    this.transferNumber = parseData?.TransferNumber;
    this.receiptForm.get("RefNo")?.setValue(parseData?.RefNo);
    this.receiptForm.get("TransferNumber")?.setValue(parseData?.TransferNumber);

  }

  async CapturePhoto() {
    const capturedImage = await this.photoservice.addNewToGallery();
    this.receiptForm.get("DR_Attachment")?.setValue(capturedImage);
  }

  async clearImage() {
    this.photoservice.photos.splice(0, 1);
  }

  saveDeliveryReceipt() {

    this.presentLoader();

    this.trListservice.attachDeliveryReceipt([this.receiptForm.value], this.token)
      .subscribe({
        next: (res: any) => {
          if (res.statusCode !== "2000") {
            return;
          }

          this.trListservice.saveToAcumatica(this.transferNumber);

        },
        error: (error: Error) => {
          this.trListservice.presentAlertError("System Error!", error.message);
        },
        complete: () => { }
      });
  }


  verifySave() {
    Swal.fire({
      title: "Receive Unit?",
      text: "Submit Delivery Receipt attacment? ",
      icon: "question",
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Confirm',
      showCancelButton: true,
    }).then((res) => {
      if (res.isConfirmed) {
        this.saveDeliveryReceipt();
      }
    });
  }

  presentLoader() {
    Swal.fire({
      title: "Saving Delivery Receipt ...",
      heightAuto: false,
      allowOutsideClick: false,
      showConfirmButton: false,
      didOpen: () => {
        Swal.showLoading();
      }
    });
  }


}
