import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { Storage } from '@ionic/storage';
import { SyncDataService } from 'src/app/services/sync-data/sync-data.service';
import Swal from 'sweetalert2';
import { DeviceStorage } from 'src/global';

@Component({
  selector: 'app-sync-mode',
  templateUrl: './sync-mode.page.html',
  styleUrls: ['./sync-mode.page.scss'],
})
export class SyncModePage implements OnInit, OnDestroy {
  public subscription = new Subscription();
  public EmployeeName: any;
  private isCurrentView: boolean = true;

  public progress = 0;
  public buffer = 0;

  resultData: boolean = false;

  progressLoad: any;
  message: any;
  messagelist = [
    'Getting Connected to the Server',
    'Syncing to Your Device',
    'Please Do not close the App',
    'Data Sync almost Complete'
  ];

  constructor(
    private nav: NavController,
    private storage: Storage,
    private sync: SyncDataService,
    private platform: Platform,
  ) {
    this.subscription.add(
      this.platform.backButton.subscribeWithPriority(9999, (processNextHandler) => {
        if (this.isCurrentView) {
          return;
        } else {
          processNextHandler();
        }
      })
    )
  }

  async ngOnInit() {
    this.syncingData();
  }

  async syncingData() {
    this.progressLoad = setInterval(() => {
      this.buffer += 0.06;
      this.progress += 0.05;

      if (this.progress > 1) {
        this.message = '';
        clearInterval(this.progressLoad);
        this.presentAlert();
      }
      else if (this.progress >= 0.06 && this.progress < 0.35) {
        this.message = this.messagelist[0];
        this.loadAddress();
        this.loadBranchList();
      }
      else if (this.progress >= 0.35 && this.progress < 0.70) {
        this.message = this.messagelist[1];
        this.loadOccupation();
        this.loadActivity();
        this.loadResultCode();
      }
      else if (this.progress >= 0.70 && this.progress < 0.9) {
        this.message = this.messagelist[2];
        this.loadProducts();
        this.loadProductModel();
        this.loadProductBrand();
      }
      else if (this.progress >= 0.90) {
        this.message = this.messagelist[3];
        this.loadAdvertisements();
        this.loadAgents();
        this.loadCorporateAccount();
      }

    }, 1000);
  }


  async loadActivity() {
    this.storage.get(DeviceStorage.UserInformation).then((res: any) => {
      const getMKTGActivities = this.sync.getActivities(res.employeeNo, res.token, 'SAMS').subscribe({
        next: (result: any) => {
          if (result.statusCode !== '2000') {
            return;
          }
          this.storage.set(DeviceStorage.ActivityList, result);
        },
        error: (error: any) => { },
        complete: () => { }
      });
      this.subscription.add(getMKTGActivities);
    });
  }

  async loadAddress() {
    this.storage.get(DeviceStorage.UserInformation).then((res: any) => {
      const getProvince = this.sync.getProvince(res.employeeNo, res.token).subscribe({
        next: (result: any) => {
          if (result.statusCode !== '2000') {
            return;
          }
          this.storage.set(DeviceStorage.ProvinceList, result);
        },
        error: (error: any) => { },
        complete: () => { }
      });

      const getCity = this.sync.getCity(res.employeeNo, res.token).subscribe({
        next: (result: any) => {
          if (result.statusCode !== '2000') {
            return;
          }
          this.storage.set(DeviceStorage.CityList, result);
        },
        error: (error: any) => { },
        complete: () => { }
      });

      const getBarangay = this.sync.getBarangay(res.employeeNo, res.token).subscribe({
        next: (result: any) => {
          if (result.statusCode !== '2000') {
            return;
          }
          this.storage.set(DeviceStorage.BarangayList, result);
        },
        error: (error: any) => { },
        complete: () => { }
      });

      this.subscription.add(getProvince);
      this.subscription.add(getCity);
      this.subscription.add(getBarangay);
    });


  }

  async loadOccupation() {
    this.storage.get(DeviceStorage.UserInformation).then((res: any) => {
      const getOccupation = this.sync.getOccupation(res.employeeNo, res.token).subscribe({
        next: (result: any) => {
          if (result.statusCode !== '2000') {
            return;
          }
          this.storage.set(DeviceStorage.OccupationList, result);
        },
        error: (error: any) => { },
        complete: () => { }
      });
      this.subscription.add(getOccupation);
    });
  }

  async loadProducts() {
    this.storage.get(DeviceStorage.UserInformation).then((res: any) => {
      const getProduct = this.sync.getProduct(res.employeeNo, res.token).subscribe({
        next: (result: any) => {
          if (result.statusCode !== '2000') {
            return;
          }
          this.storage.set(DeviceStorage.ProductList, result);
        },
        error: (error: any) => { },
        complete: () => { }
      });
      this.subscription.add(getProduct);
    });
  }


  async loadProductModel() {
    this.storage.get(DeviceStorage.UserInformation).then((res: any) => {
      const getProductModel = this.sync.getModel(res.employeeNo, res.token).subscribe({
        next: (result: any) => {
          if (result.statusCode !== '2000') {
            return;
          }
          this.storage.set(DeviceStorage.ProductModelList, result);
        },
        error: (error: any) => { },
        complete: () => { }
      });
      this.subscription.add(getProductModel);
    });
  }


  async loadProductBrand() {
    this.storage.get(DeviceStorage.UserInformation).then((res: any) => {
      const getBrand = this.sync.getBrand(res.employeeNo, res.token).subscribe({
        next: (result: any) => {
          if (result.statusCode !== '2000') {
            return;
          }
          this.storage.set(DeviceStorage.ProductBrandList, result);
        },
        error: (error: any) => { },
        complete: () => { }
      });

      this.subscription.add(getBrand);
    });
  }


  async loadAdvertisements() {
    this.storage.get(DeviceStorage.UserInformation).then((res: any) => {
      const getAdvertisements = this.sync.getAdvertisements(res.employeeNo, res.token).subscribe({
        next: (result: any) => {
          if (result.statusCode !== '2000') {
            return;
          }
          this.storage.set(DeviceStorage.ClientSourceList, result);
        },
        error: (error: any) => { },
        complete: () => { }
      });
      this.subscription.add(getAdvertisements);
    });
  }


  async loadAgents() {
    this.storage.get(DeviceStorage.UserInformation).then((res: any) => {
      const getAgents = this.sync.getAgents(res.employeeNo, res.token).subscribe({
        next: (result: any) => {
          if (result.statusCode !== '2000') {
            return;
          }
          this.storage.set(DeviceStorage.AgentList, result);
        },
        error: (error: any) => { },
        complete: () => { }
      });

      this.subscription.add(getAgents);
    });
  }


  async loadCorporateAccount() {
    this.storage.get(DeviceStorage.UserInformation).then((res: any) => {
      const getCorporateAccounts = this.sync.getCorporateAccounts(res.employeeNo, res.token).subscribe({
        next: (result: any) => {
          if (result.statusCode !== '2000') {
            return;
          }
          this.storage.set(DeviceStorage.CorporateAccountList, result);
        },
        error: (error: any) => { },
        complete: () => { }
      });
      this.subscription.add(getCorporateAccounts);
    });
  }


  async loadResultCode() {
    this.storage.get(DeviceStorage.UserInformation).then((res: any) => {
      const getResultCode = this.sync.getResultCode(res.employeeNo, res.token)
        .subscribe({
          next: (result: any) => {
            if (result.statusCode !== '2000') {
              return;
            }
            this.storage.set(DeviceStorage.ResultCodeList, result);
          },
          error: (error: any) => { },
          complete: () => { }
        });
      this.subscription.add(getResultCode);
    });
  }

  async loadBranchList() {
    this.storage.get(DeviceStorage.UserInformation).then((res: any) => {
      const getBranchList = this.sync.getBranchList(res.token)
        .subscribe({
          next: (result: any) => {
            if (result.statusCode !== '2000') {
              return;
            }
            this.storage.set(DeviceStorage.BranchList, result);
          },
          error: (error: Error) => {
          },
          complete: () => { }
        });
      this.subscription.add(getBranchList);
    });
  }


  async presentAlert() {
    Swal.fire({
      title: 'Sync Finished!',
      text: 'Data successfully Synced!',
      icon: 'success',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Continue',
    }).then((result) => {
      if (result.isConfirmed) {
        this.storage.set(DeviceStorage.AppInitialize, true)
        this.nav.navigateForward('/tabs');
      }
    });
  }


  async ngOnDestroy() {
    clearInterval(this.progressLoad);
  }

}
