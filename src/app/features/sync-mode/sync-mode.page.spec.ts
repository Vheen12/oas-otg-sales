import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SyncModePage } from './sync-mode.page';
import { IonicStorageModule, Storage } from '@ionic/storage-angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('SyncModePage', () => {
  let component: SyncModePage;
  let fixture: ComponentFixture<SyncModePage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [SyncModePage],
      imports: [
        IonicModule.forRoot(),
        IonicStorageModule,
        HttpClientTestingModule
      ],
      providers: [Storage],
    }).compileComponents();

    fixture = TestBed.createComponent(SyncModePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
