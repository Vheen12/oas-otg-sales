import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SyncModePageRoutingModule } from './sync-mode-routing.module';

import { SyncModePage } from './sync-mode.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SyncModePageRoutingModule
  ],
  declarations: [SyncModePage]
})
export class SyncModePageModule {}
