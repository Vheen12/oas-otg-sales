import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { TrListService } from 'src/app/services/tr-list/tr-list.service';
import { DeviceStorage } from 'src/global';

@Component({
  selector: 'app-tr-list',
  templateUrl: './tr-list.page.html',
  styleUrls: ['./tr-list.page.scss'],
})
export class TrListPage implements OnInit {

  loaded: boolean = false;
  token: string = "";
  pdiNumber: number = 100000;
  transferReceiptList: any[] = [];
  dateToday: Date = new Date();

  constructor(
    private router: Router,
    private storage: Storage,
    private trservice: TrListService,
  ) { }

  ngOnInit() {
    this.getUserInfo();

  }

  getUserInfo() {
    this.storage
      .get(DeviceStorage.UserInformation)
      .then((res: any) => {
        this.token = res?.token;
        this.getStackTransferList(res?.token, res?.branchCode,res?.userId);
      });
  }

  getStackTransferList(token: string, branch: string, userID: string) {
    this.trservice.getTransferReceiptList(token)
      .subscribe({
        next: (res: any) => {

          if (res.statusCode !== "2000") {
            return;
          }

          if (userID=== '210000997') {
            this.transferReceiptList = res.data;
            return;
          }

          this.transferReceiptList = res.data.filter((element: any) => {
            return element.Branch === branch;
          });

        },
        error: (error: Error) => {
          this.trservice
            .presentAlertError("Oops!", error.message);
          this.loaded = true;

        },
        complete: () => {
          this.loaded = true;
        },
      })
  }

  doRefresh(event: any) {
    this.loaded = false;
    setTimeout(() => {
      this.getUserInfo();
      event.target.complete();
    }, 2000);
  }

  goToBarcodeScanner(item: any) {
    localStorage.setItem("transfer_receipt", JSON.stringify(item));
    this.router.navigateByUrl("qr-scanner");
  }

}
