import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TrListPageRoutingModule } from './tr-list-routing.module';

import { TrListPage } from './tr-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TrListPageRoutingModule
  ],
  declarations: [TrListPage]
})
export class TrListPageModule {}
