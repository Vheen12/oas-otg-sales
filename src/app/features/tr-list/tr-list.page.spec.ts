import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TrListPage } from './tr-list.page';

describe('TrListPage', () => {
  let component: TrListPage;
  let fixture: ComponentFixture<TrListPage>;

  beforeEach(waitForAsync () => {
    fixture = TestBed.createComponent(TrListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
