import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TrListPage } from './tr-list.page';

const routes: Routes = [
  {
    path: '',
    component: TrListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TrListPageRoutingModule {}
