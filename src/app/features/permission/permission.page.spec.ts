import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { PermissionPage } from './permission.page';

describe('PermissionPage', () => {
  let component: PermissionPage;
  let fixture: ComponentFixture<PermissionPage>;

  beforeEach(waitForAsync () => {
    fixture = TestBed.createComponent(PermissionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
