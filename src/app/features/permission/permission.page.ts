import { Component, OnInit } from '@angular/core';
import { GeolocationService } from 'src/app/services/geolocation/geolocation.service';
import { PermissionService } from './permission.service';
import { Camera } from '@capacitor/camera';
import { Filesystem } from '@capacitor/filesystem';
location
@Component({
  selector: 'app-permission',
  templateUrl: './permission.page.html',
  styleUrls: ['./permission.page.scss'],
})
export class PermissionPage implements OnInit {

  public permissionList = [
    { id: 0, label: "Camera", icon: "camera", status: false, details: "Permission to Take Pictures using a Device" },
    { id: 1, label: "Location", icon: "location", status: false, details: "For Position Monitoring and Geofencing" },
    { id: 2, label: "Media and Files", icon: "folder", status: false, details: "For Features Related to Files and Media" },
  ];

  pageLoaded: boolean = false;

  constructor(
    private geolocationservice: GeolocationService,
    private permissionService: PermissionService,
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.checkAllPermissions();

    setTimeout(() => {
      this.pageLoaded = true;
    }, 2000);
  }

  checkAllPermissions() {
    this.checkLocationPermission();
    this.checkCameraPermission();
    this.checkFileSystemPermission();
  }

  doRefresh(event: any) {
    this.pageLoaded = false;

    this.permissionList.forEach((row: any) => {
      row.status = false;
    });

    this.checkAllPermissions();

    setTimeout(() => {
      event.target.complete();
      this.pageLoaded = true;
    }, 2000);
  }

  requestPermission(id: number, event: any) {
    let checkStatus: boolean = event.detail.checked;

    if (checkStatus) {
      switch (id) {
        case 0:
          this.requestCameraPermission();
          break;
        case 1:
          this.requestLocationPermission();
          break;
        case 2:
          this.requestFileSystemPermission();
          break;

        default:
          break;
      }
    }

    else {
      this.permissionList[id].status = true;
    }
  }


  checkLocationPermission = async () => {
    await this.geolocationservice.checkLocationPermission()
      .then((res: string) => {
        if (res !== "granted") {
          return;
        }

        this.permissionList[1].status = true;

      });
  }


  requestLocationPermission = async () => {
    this.geolocationservice.requestLocationPermission()
      .then((res: any) => {
        return;

      })
      .catch((error: Error) => {
        this.permissionService
          .presentAlertError('Permission Error!', error?.message);
      });
  }


  checkCameraPermission = async () => {
    Camera.checkPermissions()
      .then((res: any) => {

        if (res?.camera !== "granted") {
          return;
        }

        this.permissionList[0].status = true;
      })
      .catch((error: Error) => {
        this.permissionService
          .presentAlertError('Permission Error!', error?.message);
      });
  }


  requestCameraPermission = async () => {
    Camera.requestPermissions()
      .then((res) => {
        return;
      })
      .catch((error: Error) => {
        this.permissionService
          .presentAlertError('Permission Error!', error?.message);
      });
  }


  checkFileSystemPermission = async () => {
    Filesystem.checkPermissions()
      .then((res: any) => {
        if (res?.publicStorage !== "granted") {
          return;
        }

        this.permissionList[2].status = true;
      })
      .catch((error: Error) => {
        this.permissionService
          .presentAlertError('Permission Error!', error?.message)
      });
  }


  requestFileSystemPermission = async () => {
    Filesystem.checkPermissions()
      .then((res: any) => {
        return
      })
      .catch((error: Error) => {
        this.permissionService
          .presentAlertError('Permission Error!', error?.message)
      });
  }
}
