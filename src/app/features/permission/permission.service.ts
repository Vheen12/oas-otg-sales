import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class PermissionService {

  constructor() { }

  presentAlertError(header: string, message: string) {
    Swal.fire({
      title: header,
      text: message,
      icon: "error",
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Continue',
    });
  }
}
