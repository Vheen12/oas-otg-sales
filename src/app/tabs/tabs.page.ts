import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { NetworkConnectionService } from '../services/network-connection/network-connection.service';
import Swal from 'sweetalert2';
import { DeviceStorage } from 'src/global';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

  public userAccess: string = "";

  constructor(
    private network: NetworkConnectionService,
    private router: Router,
    private storage: Storage,
  ) { }

  ngOnInit() { }

  ionViewWillEnter() {
    this.storage.get(DeviceStorage.UserAccess)
      .then((res: string) => {
        this.userAccess = res;
      });
  }

  proceedToTimeKeeping() {
    this.network.checkNetworkConnection().then((res: boolean) => {
      if (!res) {
        this.presentOfflineAlert();
        return;
      }
      this.router.navigateByUrl("timekeeping");

    })
  }

  async presentOfflineAlert() {
    Swal.fire({
      title: 'No internet connection detected!',
      text: 'Please connect to the internet to time in.!',
      icon: 'error',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonText: 'Retry',
      confirmButtonColor: '#0e8140',
      showCancelButton: true,
    }).then((result) => {
      if (result.isConfirmed) {
        this.proceedToTimeKeeping();
      }
    });
  }

}
