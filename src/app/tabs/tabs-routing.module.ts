import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children:[
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'home',
      },
      {
        path: 'home',
        loadChildren: () => import('../core/home/home.module').then((m) => m.HomePageModule),
      },
      {
        path: 'home-bao',
        loadChildren: () => import('../core/home-bao/home-bao.module').then((m) => m.HomeBaoPageModule),
      },
      {
        path: 'asq-list',
        loadChildren: () => import('../core/asq-list/asq-list.module').then(m => m.AsqListPageModule)
      },
      {
        path: 'workschedule',
        loadChildren: () => import('../core/workschedule/workschedule.module').then(m => m.WorkschedulePageModule),
      },
      {
        path: 'timekeeping',
        loadChildren: () => import('../core/timekeeping/timekeeping.module').then(m => m.TimekeepingPageModule),
      },
      {
        path: 'prospect-profile-form',
        loadChildren: () => import('../core/prospect-profile-form/prospect-profile-form.module').then(m => m.ProspectProfileFormPageModule),
      },
      {
        path: 'user-account',
        loadChildren: () => import('../core/user-account/user-account.module').then( m => m.UserAccountPageModule)
      },
      {
        path: 'qr-scanner',
        loadChildren: () => import('../features/qr-scanner/qr-scanner.module').then( m => m.QrScannerPageModule)
      },
      {
        path: 'tr-list',
        loadChildren: () => import('../features/tr-list/tr-list.module').then( m => m.TrListPageModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
