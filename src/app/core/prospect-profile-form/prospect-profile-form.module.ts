import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProspectProfileFormPageRoutingModule } from './prospect-profile-form-routing.module';

import { ProspectProfileFormPage } from './prospect-profile-form.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProspectProfileFormPageRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [ProspectProfileFormPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProspectProfileFormPageModule {}
