import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ProspectProfileFormPage } from './prospect-profile-form.page';
import { IonicStorageModule, Storage } from '@ionic/storage-angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ProspectProfileFormPage', () => {
  let component: ProspectProfileFormPage;
  let fixture: ComponentFixture<ProspectProfileFormPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ProspectProfileFormPage],
      imports: [
        IonicStorageModule,
        HttpClientTestingModule
      ],
      providers: [Storage]
    }).compileComponents();

    fixture = TestBed.createComponent(ProspectProfileFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
