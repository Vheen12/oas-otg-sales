import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProspectProfileFormPage } from './prospect-profile-form.page';

const routes: Routes = [
  {
    path: '',
    component: ProspectProfileFormPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProspectProfileFormPageRoutingModule {}
