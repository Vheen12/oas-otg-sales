import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AsqListPageRoutingModule } from './asq-list-routing.module';

import { AsqListPage } from './asq-list.page';
import { AngularSignaturePadModule } from '@almothafar/angular-signature-pad';
import { PdfViewerModule } from 'ng2-pdf-viewer';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AsqListPageRoutingModule,
    AngularSignaturePadModule,
    ReactiveFormsModule,
    PdfViewerModule,
  ],
  declarations: [AsqListPage]
})
export class AsqListPageModule { }
