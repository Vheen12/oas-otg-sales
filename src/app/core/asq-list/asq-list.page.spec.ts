import { IonicStorageModule, Storage } from '@ionic/storage-angular';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AsqListPage } from './asq-list.page';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('AsqListPage', () => {
  let component: AsqListPage;
  let fixture: ComponentFixture<AsqListPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [IonicStorageModule, HttpClientTestingModule],
      providers: [Storage],
      declarations: [AsqListPage]
    }).compileComponents();

    fixture = TestBed.createComponent(AsqListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
