import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AsqListPage } from './asq-list.page';

const routes: Routes = [
  {
    path: '',
    component: AsqListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AsqListPageRoutingModule {}
