import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DeviceStorage, URL_STRING } from 'src/global';
import { Storage } from '@ionic/storage';
import { NgSignaturePadOptions, SignaturePadComponent } from '@almothafar/angular-signature-pad';
import Swal from 'sweetalert2';
import { Browser } from '@capacitor/browser';
import { Router } from '@angular/router';
import { FilePicker, PickFilesResult } from '@capawesome/capacitor-file-picker';

import { SyncDataService } from 'src/app/services/sync-data/sync-data.service';
import { NetworkConnectionService } from 'src/app/services/network-connection/network-connection.service';
import { QuotationService } from 'src/app/services/quotation/quotation.service';
import { PhotoService } from 'src/app/services/photo/photo.service';
import { GeolocationService } from 'src/app/services/geolocation/geolocation.service';

export interface Coordinates {
  latitude: number;
  longitude: number;
}

@Component({
  selector: 'app-asq-list',
  templateUrl: './asq-list.page.html',
  styleUrls: ['./asq-list.page.scss'],
})
export class AsqListPage implements OnInit, OnDestroy {
  @ViewChild('signature')
  public signaturePad: SignaturePadComponent;

  signaturePadOptions: NgSignaturePadOptions = {
    minWidth: 5,
    canvasWidth: window.innerWidth - 35,
    canvasHeight: 250,
    backgroundColor: 'transparent',
  };

  asqForm: FormGroup;
  loaded: boolean = false;
  isModalOpen: boolean = false;
  showSignaturePad: boolean = false;
  token: string = "";
  quotationList: any[] = [];
  quotationViewList: any[] = [];

  segmentValue: string = 'attachment';
  financeType: string = "";
  videoSource: string = "";
  videoOptions: any;

  // * Geotagging Images Properties
  attachmentData: string = "";
  imageGeoTag: any;
  imageDateTimeTag: any;
  LocationAddress: string = "";
  geofencingStatus: string = "";

  coordinatesData: Coordinates;

  constructor(
    private storage: Storage,
    private service: SyncDataService,
    private quotationservice: QuotationService,
    private formBuilder: FormBuilder,
    private router: Router,
    private network: NetworkConnectionService,
    private geolocation: GeolocationService,
    public photoservice: PhotoService
  ) {
    this.asqForm = this.formBuilder.group({
      Ref_No: [''],
      Signature: ['', Validators.required],
      CLI_Code: [''],
      Status: ['SIGNED QUOTATION'],
      DateSigned: [''],
      Attachment: ['', Validators.required],
      Video: ['', Validators.required]
    })
  }

  ngOnInit() { }
  ngOnDestroy(): void {

  }

  ionViewWillEnter() {
    this.checkNetworkStatus();
  }

  doRefresh(event: any) {
    this.loaded = false;
    this.quotationList = [];

    setTimeout(() => {
      this.getUserInfo();
      event.target.complete();
    }, 2000);
  }

  getUserInfo = async () => {
    this.storage.get(DeviceStorage.UserInformation).then((res: any) => {
      this.token = res.token;
      this.getASQList(res);
    });
  }

  checkNetworkStatus() {
    this.network.checkNetworkConnection().then((res: boolean) => {
      if (!res) {
        this.presentOfflineAlert();
        return;
      }

      this.getUserInfo();

    });
  }

  getASQList = async (res: any) => {
    this.service.getASQList(res.userId, res.token)
      .subscribe({
        next: (res: any) => {
          if (res.statusCode !== '2000') {
            this.loaded = true;
            return;
          }

          console.log(res.data)
          this.quotationList = res.data;
          this.quotationViewList = res.data;

          this.getPendingQuotation();

          this.storage.set(DeviceStorage.QuotationList, res);
          this.loaded = true;
        },
        error: (error: any) => {
          this.quotationservice.presentAlertError('ASQ: Fetching ASQ', error.message);
          this.loaded = true;
        },
      });
  }


  saveSignedQuotation = async () => {

    let incomingdata: any[] = [this.asqForm.value];

    if (this.financeType === "IN HOUSE") {
      this.quotationservice.attachSignatureWithVideo(incomingdata, this.token).subscribe({
        next: (res: any) => {
          if (res.statusCode !== '2000') {
            this.quotationservice.presentAlertError("Attach Signature!", res.message);
            return;
          }
          this.presentSuccessAlert();
        },
        error: (error: any) => {
          this.quotationservice.presentAlertError("Attach Signature Error!", error.message);
        },
        complete: () => { },
      });

      return;
    }

    this.quotationservice.attachSignature(incomingdata, this.token).subscribe({
      next: (res: any) => {
        if (res.statusCode !== '2000') {
          this.quotationservice.presentAlertError("Attach Signature!", res.message);
          return;
        }
        this.presentSuccessAlert();
      },
      error: (error: any) => {
        this.quotationservice.presentAlertError("Attach Signature Error!", error.message);
      },
      complete: () => { },
    });
  }

  openSignaturePad = (data: any) => {
    this.financeType = data.ARML_Finance_Type;

    if(this.financeType !== "IN HOUSE"){
      this.asqForm.get("Video").setValue("NA");
    }
    this.isModalOpen = true;
    this.asqForm.get("Ref_No")?.setValue(data.ARML_AR_No);
    this.asqForm.get("CLI_Code")?.setValue(data.CLI_Code);
  }

  closeSignatureModal = async () => {
    this.isModalOpen = false;
    this.segmentValue = "attachment";
    this.clearImage();
    this.clearSelectedVideo();
    this.asqForm.get("Signature")?.setValue("");
  }


  private presentSuccessAlert() {
    Swal.fire({
      title: 'Success!',
      text: 'Quotation Signed!',
      icon: 'success',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Continue',
    }).then((result) => {
      if (result.isConfirmed) {
        this.isModalOpen = false;
      }
    });
  }

  private presentOfflineAlert() {
    Swal.fire({
      title: 'No internet connection detected!',
      text: 'Please connect to the internet to time in.!',
      icon: 'error',
      heightAuto: false,
      allowOutsideClick: false,
      showCancelButton: true,
      confirmButtonText: 'Retry',
      confirmButtonColor: '#0e8140',
      cancelButtonText: 'Back to Home',
      cancelButtonColor: 'red'
    }).then((result) => {
      if (result.isConfirmed) {
        this.checkNetworkStatus();
      } else {
        this.router.navigateByUrl('/tabs/home');
      }
    });
  }

  drawComplete(event: MouseEvent | Touch) {
    // will be notified of szimek/signature_pad's onEnd event
    console.log('Completed drawing', event);

    const ESIGNATURE = (this.signaturePad.toDataURL("png")).slice(22);
    const DATE_TODAY = new Date();

    this.asqForm.get("Signature")?.setValue(ESIGNATURE);
    this.asqForm.get("DateSigned")?.setValue(DATE_TODAY);
  }

  drawStart(event: MouseEvent | Touch) {
    // will be notified of szimek/signature_pad's onBegin event
    console.log('Start drawing', event);
  }

  clearSignaturePad = async () => {
    this.signaturePad.clear();
    this.asqForm.get("Signature")?.setValue("");
  }

  enableSignaturePad = async (event: any) => {
    const checked = event.detail.checked;

    checked ? this.showSignaturePad = true : this.showSignaturePad = false;
  }

  async printQuotation(data: any) {
    await Browser
      .open({ url: `${URL_STRING.BASE_URL}/otgLogin/${data.ARML_AR_No}?method=dkas/paya_pdf&token=${this.token}` });
  }

  async viewQuotation(data: any) {
    await Browser
      .open({ url: `${URL_STRING.BASE_URL}/otgLogin/${data.ARML_AR_No}?method=fmr/detailed_paya&token=${this.token}` });
  }


  filterQuotationList(event: any) {
    let filter: string = event.detail?.value;
    this.quotationList = this.quotationViewList;

    if (filter === 'DEFAULT') {
      this.getPendingQuotation();
    }
    else {
      this.quotationList = this.quotationList.filter((data: any) => {
        return data?.ARML_Status === filter;
      });
    }
  }

  private getPendingQuotation() {
    this.quotationList = this.quotationList.filter((data: any) => {
      return data?.ARML_Status !== "SIGNED QUOTATION" && data?.ARML_Status !== "CEO APPROVED";
    });
  }

  segmentChanged(event: any) {
    this.segmentValue = event.detail.value;
  }

  // ! Image Attachment

  async capturePhoto() {
    const imageData = await this.photoservice.addNewToGallery();
    this.asqForm.get('Attachment')?.setValue(imageData);
    this.attachmentData = imageData;
    this.setUserGeolocation();
    this.resetVariables();
  }

  async setUserGeolocation() {
    await this.getUserCoordinates();
    await this.getUserLocation();
  }

  async clearImage() {
    this.photoservice.photos.splice(0, 1);
    this.attachmentData = "";
    this.resetVariables();
  }

  async resetVariables() {
    this.imageDateTimeTag = "";
    this.imageGeoTag = "";
    this.LocationAddress = "";
    this.geofencingStatus = "";
  }

  // ! Geolocation 


  async getUserCoordinates() {
    await this.geolocation
      .getCoordinates()
      .then((res: any) => {
        this.coordinatesData = res.coords;
        this.imageDateTimeTag = new Date();
        this.imageGeoTag = `Latitude: ${res.coords.latitude} Longitude: ${res.coords.longitude}`;
      })
      .catch((error: Error) => {
        this.quotationservice
          .presentAlertError('Geolocation Error!', error.message);
      });
  }

  async getUserLocation() {
    await this.geolocation
      .getLocation(this.coordinatesData.latitude, this.coordinatesData.longitude)
      .then((res: any) => {
        this.LocationAddress = res;
      })
      .catch((error: Error) => {
        this.quotationservice
          .presentAlertError("Reverse Geolocation Error!", error.message);
      });
  }

  // ! Video Files Manipulation

  uploadVideo() {
    FilePicker.pickVideos({ readData: true })
      .then((res: PickFilesResult) => {
        console.log(res);
        this.videoSource = res.files[0].data;
        this.videoOptions = res.files[0];
        this.asqForm.get("Video").setValue(res.files[0].data);
      });
  }

  clearSelectedVideo() {
    this.videoSource = '';
    this.asqForm.get("Video").setValue("");
  }


}
