import { Component, OnDestroy, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { NetworkConnectionService } from 'src/app/services/network-connection/network-connection.service';
import { WorkscheduleService } from 'src/app/services/workschedule/workschedule.service';
import { GeolocationService } from 'src/app/services/geolocation/geolocation.service';
import Swal from 'sweetalert2';
import { DeviceStorage } from 'src/global';

export interface Result {
  data?: string[];
  message?: string;
  statusCode?: string;
}
export interface Itinerary_Action {
  data?: any[];
  status?: string;
  timestamp?: Date;
  latitude?: number;
  longitude?: number;
  location?: any;
}
@Component({
  selector: 'app-workschedule',
  templateUrl: './workschedule.page.html',
  styleUrls: ['./workschedule.page.scss'],
})

export class WorkschedulePage implements OnInit, OnDestroy {

  private subscription = new Subscription();

  token: string = "";
  userId: string = "";
  locationPermission: string = "";

  offlineData: any;
  loadDWAOffline: any;

  branch: any;
  branchCode: string = "";
  loaded: boolean = false;
  NetworkStatus: boolean = false;

  errorCtr: number = 0;
  successCtr: number = 0;

  clientCoordinatesArr = [];

  regex = /data:.*base64,/;

  constructor(
    public workschedService: WorkscheduleService,
    private storage: Storage,
    private router: Router,
    private geolocation: GeolocationService,
    private network: NetworkConnectionService,
  ) { }

  ngOnInit() { }

  ionViewWillEnter() {
    this.checkLocationPermission();
    this.getNetworkStatus();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


  checkLocationPermission = async () => {
    await this.geolocation.checkLocationPermission()
      .then((res: string) => {

        if (res !== "granted") {
          this.requestLocationPermission();
          return;
        }

        this.locationPermission = res;
      });
  }


  requestLocationPermission = async () => {
    this.geolocation.requestLocationPermission()
      .then((res: any) => {

        this.locationPermission = res;

      }).catch((error: any) => {
        this.workschedService.presentAlertError('Permission Error!', error);
      });
  }


  async getNetworkStatus() {
    this.network.checkNetworkConnection()
      .then(async (res: boolean) => {

        this.NetworkStatus = res;

      })
      .finally(async () => await this.getUserInfo());
  }


  async getUserInfo() {
    await this.storage.get(DeviceStorage.UserInformation)
      .then((res: any) => {
        this.token = res.token;
        this.branchCode = res.branchCode;
        this.userId = res.userId;
        this.getBranchList(res);

        return res;
      })
      .then((res: any) => {
        this.NetworkStatus
          ? this.fetchItinerary(res?.userId, res?.token)
          : this.loadItineraryOffline();
      });
  }


  private getBranchList(userBranch: any) {

    this.storage.get(DeviceStorage.BranchList)
      .then((res: Result) => {
        if (!res) {
          this.workschedService
            .presentEmptyAppData("Empty Branch List!",
              "There is No Branch List Detected! Please Sync App Data to Refresh");
          return;
        }

        if (res.statusCode !== "2000") {
          this.workschedService.presentAlertError("Empty Branch List", res?.message);
          return;
        }

        this.branch = res.data.filter((item: any) => {
          return item.branchCode === userBranch.branchCode;
        });
      }).catch((error: Error) => {
        console.log(error?.message);
      });
  }

  async fetchItinerary(userID: string, token: string) {
    const GET_ITINERARY = this.workschedService.getItinerary(userID, token)
      .subscribe({
        next: (res: Result) => {
          if (res.statusCode === '-1005') {
            this.storage.remove(DeviceStorage.ItineraryList);
            this.storage.remove(DeviceStorage.Itinerary_Coordinates);
            return;
          }

          if (res.statusCode === '2000') {
            this.checkItinerary(res);
            return;
          }
        },
        error: (error: Result) => {
          this.workschedService
            .presentAlertError('Workschedule: Fetching Itinerary Error!', error.message);
        },
        complete: () => {
          this.loaded = true;
        }
      });

    this.subscription.add(GET_ITINERARY);
  }


  async checkItinerary(incomingData: any) {
    await this.storage.get(DeviceStorage.ItineraryList)
      .then((res: Result) => {
        if (res) {
          let existingData: any = res.data.every((element: any) => {
            return element.CSL_Action_Needed_By === this.userId;
          });


          if (!existingData) {
            this.storage.remove(DeviceStorage.ItineraryList);
            this.storage.set(DeviceStorage.ItineraryList, incomingData);
          }

          this.verifyItinerary(res, incomingData);
          return;
        }

        this.storeItineraries(incomingData);

      })
      .catch((error: Result) => {
        this.workschedService
          .presentAlertError('Workschedule: Set Itinerary', error.message);
      })
      .finally(async () => {
        await this.getOfflineClientCoordinates();
      });
  }


  private verifyItinerary(offlineData: any, onlineData: any) {
    // * If online data reference number is equal to offline data, online data would change to offline data
    for (let i = 0; i < offlineData.data.length; i++) {
      const element = offlineData.data[i];

      for (let j = 0; j < onlineData.data.length; j++) {
        const newData = onlineData.data[j];

        if (newData.CSL_Ref_No === element.CSL_Ref_No) {
          this.setItineraryData(newData, element);
        }
      }
    }

    this.offlineData = (onlineData.data.filter((row: any) => {
      return row.completed_Sync_status === 1;
    })).length || 0;

    this.storeItineraries(onlineData);
  }

  private setItineraryData(newData: any, element: any) {
    // * Started an Itnenary
    newData.Status = element?.Status;
    newData.CSL_Longitude_Start = element?.CSL_Longitude_Start;
    newData.CSL_Latitude_Start = element?.CSL_Latitude_Start;
    newData.CSL_Start_Date = element?.CSL_Start_Date;
    newData.CSL_Location_Time_Start = element?.CSL_Location_Time_Start;
    newData.started_Sync_Status = element?.started_Sync_Status;
    newData.ConfirmationDate = element?.ConfirmationDate;

    // * Arrived an Itnenary
    newData.CSL_Longitude_Time_In = element?.CSL_Longitude_Time_In;
    newData.CSL_Latitude_Time_In = element?.CSL_Latitude_Time_In;
    newData.CSL_Time_In_Date = element?.CSL_Time_In_Date;
    newData.CSL_Location_Time_In = element?.CSL_Location_Time_In;
    newData.arrived_Sync_Status = element?.arrived_Sync_Status;
    newData.ArrivalDate = element?.ArrivalDate;

    // * Complete an Itnenary
    newData.CSL_Longitude_Time_Out = element?.CSL_Longitude_Time_Out;
    newData.CSL_Latitude_Time_Out = element?.CSL_Latitude_Time_Out;
    newData.CSL_Time_Out_Date = element?.CSL_Time_Out_Date;
    newData.CSL_Location_Time_Out = element?.CSL_Location_Time_Out;
    newData.CSL_Result = element?.CSL_Result;
    newData.CSL_Result_Date = element?.CSL_Result_Date;
    newData.CSL_Status_To_Remarks = element?.CSL_Status_To_Remarks;
    newData.CSL_Result_Remarks = element?.CSL_Result_Remarks;
    newData.completed_Sync_status = element?.completed_Sync_status;
    newData.Attachment = element?.Attachment;
    newData.Latitude = element?.CSL_Latitude_Time_Out;
    newData.Longitude = element?.CSL_Longitude_Time_Out;
    newData.Location = element?.CSL_Location_Time_Out;
    newData.GeofenceStatus = element.GeofenceStatus;
    newData.GeofenceReason = element.GeofenceReason;
  }

  private storeItineraries(incomingData: any) {
    this.loadDWAOffline = incomingData?.data.sort(function (a, b) {
      if (a.Status < b.Status) {
        return -1;
      }
      if (a.Status > b.Status) {
        return 1;
      }
      return 0;
    });

    this.storage.set(DeviceStorage.ItineraryList, incomingData);
    this.loaded = true;
  }

  async getOfflineClientCoordinates() {
    await this.storage.get(DeviceStorage.ItineraryList)
      .then((res: any) => {
        this.clientCoordinatesArr = [];

        res.data.forEach((element: any) => {
          const clientAddress = `${element.brgy_desc}, ${element.city_desc}, ${element.province_desc}`;

          this.fetchCoordinates(clientAddress, element);
        });
      });
  }

  private fetchCoordinates(clientAddress: string, element: any) {
    this.geolocation.getAddressCoordinate(clientAddress)
      .then((res: any) => {
        let coordinates: any = {
          latitude: res?.latitude,
          longitude: res?.longitude,
        }
        res
          ? this.storeClientCoordinates(element, coordinates)
          : this.storeClientCoordinates(element, this.branch[0]);
      }).catch((error: Error) => {
        this.workschedService
          .presentAlertError("Fetching Client Coordinates Error", error.message);
      }).finally(() => {
        this.storage
          .set(DeviceStorage.Itinerary_Coordinates, this.clientCoordinatesArr);
      });
  }

  // private fetchCoordinates(clientAddress: string, element: any) {
  //   this.geolocation.getAddressCoordinates(clientAddress).subscribe({
  //     next: (res: any) => {
  // if (res) {
  //   let coordinates: any = {
  //     latitude: res?.[0]?.lat,
  //     longitude: res?.[0]?.lon,
  //   }
  //   res.length > 0
  //     ? this.storeClientCoordinates(element, coordinates)
  //     : this.storeClientCoordinates(element, this.branch[0]);
  // }
  //     },
  //     error: (error: Error) => {
  // this.workschedService
  //   .presentAlertError("Fetching Client Coordinates Error", error.message);
  //     },
  //     complete: () => {
  // this.storage
  //   .set(DeviceStorage.Itinerary_Coordinates, this.clientCoordinatesArr);
  //     }
  //   });
  // }


  private storeClientCoordinates(element: any, coordinates: any) {
    let rawData: any = {
      CSL_Ref_No: element.CSL_Ref_No,
      CLI_Latitude: +coordinates?.latitude,
      CLI_Longitude: +coordinates?.longitude,
    };

    this.clientCoordinatesArr.push(rawData);

  }

  async loadItineraryOffline() {
    await this.storage.get(DeviceStorage.ItineraryList)
      .then((res: any) => {
        if (res) {
          this.loadDWAOffline = res.data;

          this.offlineData = (res.data.filter((row: any) => {
            return row.completed_Sync_status === 1;
          })).length || 0;
        }
        this.loaded = true;

      })
      .finally(async () => {
        await this.getClientCoordinates();
      });
  }


  async getClientCoordinates() {
    await this.storage.get(DeviceStorage.Itinerary_Coordinates)
      .then((res: any) => {
        this.clientCoordinatesArr.push(res);
      });
  }



  // TODO: Functions for Doing Itinerary

  doItinerary(itineraryStatus: string, clientData: any) {
    let itineraryData: any = { itineraryStatus, clientData };

    this.verifyTimeIn(itineraryData);
  }


  verifyTimeIn(itineraryData: any) {
    let timein: string = localStorage.getItem("timeInStatus");
    (timein !== "0")
      ? this.workschedService.presentNoTimeInAlert()
      : this.getUserCoordinates(itineraryData);
  }


  getUserCoordinates(itineraryData: any) {
    this.geolocation.getCoordinates()
      .then((res: any) => {

        let newItineraryData: Itinerary_Action = {
          status: itineraryData?.itineraryStatus,
          data: itineraryData?.clientData,
          timestamp: new Date(res?.timestamp),
          latitude: res?.coords?.latitude,
          longitude: res?.coords?.longitude,
        }

        this.checkItineraryStatus(newItineraryData);

      });
  }


  checkItineraryStatus(itineraryData: Itinerary_Action) {
    switch (itineraryData?.status) {
      case 'Pending':
        this.presentItineraryStatusAlert(itineraryData, 'Are you Sure to Start this Itnerary?');
        break;
      case 'Started':
        this.presentItineraryStatusAlert(itineraryData, 'Did you Arrived in the destination?');
        break;
      case 'Arrived':
        localStorage.setItem('client_info', JSON.stringify(itineraryData?.data));
        this.router.navigateByUrl('/worksched-client');
        break;
      default:
        break;
    }
  }


  presentItineraryStatusAlert(itineraryData: any, alertMessage: string) {
    Swal.fire({
      title: 'Info',
      text: alertMessage,
      icon: 'info',
      heightAuto: false,
      allowOutsideClick: false,
      showCancelButton: true,
      confirmButtonColor: '#0e8140',
      confirmButtonText: (itineraryData?.status) === "Pending" ? 'Start' : 'Arrived',
    }).then((result) => {
      if (result.isConfirmed) this.getLocation(itineraryData);
    });
  }


  async getLocation(itineraryData: Itinerary_Action) {
    await this.geolocation
      .getLocation(itineraryData?.latitude, itineraryData?.longitude)
      .then((res: any) => {

        let incomingData: any = {
          status: (itineraryData?.status === "Pending" ? 'Started' : 'Arrived'),
          location: res,
          latitude: itineraryData?.latitude,
          longitude: itineraryData?.longitude,
          data: itineraryData?.data,
          timestamp: itineraryData?.timestamp,
        };

        itineraryData?.status === "Pending"
          ? this.doItineraryStart(incomingData)
          : this.doItineraryArrived(incomingData);
      });
  }


  // ! Do Started Itinerary
  doItineraryStart(incomingData: Itinerary_Action) {

    let clientData: any = incomingData?.data;

    const resultData = [{
      ConfirmationDate: incomingData?.timestamp,
      Latitude: incomingData?.latitude,
      Longitude: incomingData?.longitude,
      Location: incomingData?.location,
      CSL_Ref_No: clientData?.CSL_Ref_No,
    }];

    (this.NetworkStatus)
      ? this.saveStartedItinerary(resultData, incomingData)
      : this.updateItineraryStorage(incomingData, 1);
  }


  // ! Do Arrived Itinerary
  doItineraryArrived(incomingData: any) {

    let clientData: any = incomingData?.data;

    const resultData = [{
      ArrivalDate: incomingData?.timestamp,
      Latitude: incomingData?.latitude,
      Longitude: incomingData?.longitude,
      Location: incomingData?.location,
      CSL_Ref_No: clientData?.CSL_Ref_No,
    }];

    (this.NetworkStatus)
      ? this.saveArrivedItinerary(resultData, incomingData)
      : this.updateItineraryStorage(incomingData, 1);
  }

  // ! Save Started Itinerary
  saveStartedItinerary(result: any, incomingData: any) {
    this.workschedService.showActionLoading();

    const ITINERARY_STARTED = this.workschedService
      .startItnenary(result, this.token)
      .subscribe({
        next: (res: any) => {
          if (res.statusCode !== '2000') {
            this.workschedService
              .presentAlertError("Start Itinerary", res.message);
            return;
          }

          this.updateItineraryStorage(incomingData, 0);

        },
        error: (err: any) => {
          this.workschedService
            .presentAlertError("Start Itinerary Error", err.message);
        },
      });

    this.subscription.add(ITINERARY_STARTED);

  }

  // ! Save Arrived Itinerary
  saveArrivedItinerary(result: any, incomingData: any) {
    this.workschedService.showActionLoading();

    const ITINERARY_ARRIVED = this.workschedService.arrivedItnenary(result, this.token).subscribe({
      next: (res: any) => {
        if (res.statusCode !== '2000') {
          this.workschedService
            .presentAlertError("Workschedule: Arrived Itinerary", res.message);
          return;
        }

        this.updateItineraryStorage(incomingData, 0);

      },
      error: (err: any) => {
        this.workschedService
          .presentAlertError("Workschedule: Arrived Itinerary Error", err.message);
      },
    });

    this.subscription.add(ITINERARY_ARRIVED);

  }


  // ! Update Itinerary Storage
  updateItineraryStorage(incomingData: any, mode: number) {
    this.storage.get(DeviceStorage.ItineraryList)
      .then((res: any) => {

        let clientdata: any = incomingData?.data;

        let updatedData: any = res?.data.filter((element: any) => {

          (incomingData?.status === "Started")
            ? this.updateStartedItinerary(element, clientdata, incomingData, mode)
            : this.updateArrivedItinerary(element, clientdata, incomingData, mode);

          return element;

        });

        let dataToStore: any = {
          message: "SUCCESS",
          statusCode: "2000",
          data: updatedData,
        }

        this.loadDWAOffline = updatedData;
        this.storage.set(DeviceStorage.ItineraryList, dataToStore);
        this.workschedService.presentAlertSuccess(`Itinerary ${incomingData?.status}!`);
      });
  }


  updateStartedItinerary(element: any, clientdata: any, incomingData: any, mode: number) {
    if (element?.CSL_Ref_No === clientdata?.CSL_Ref_No) {
      element.Status = incomingData?.status;
      element.CSL_Longitude_Start = incomingData?.longitude;
      element.CSL_Latitude_Start = incomingData?.latitude;
      element.CSL_Start_Date = incomingData?.timestamp;
      element.CSL_Location_Time_Start = incomingData?.location;
      element.ConfirmationDate = incomingData?.timestamp;
      element.started_Sync_Status = mode;
    }
  }

  updateArrivedItinerary(element: any, clientdata: any, incomingData: any, mode: number) {
    if (element?.CSL_Ref_No === clientdata?.CSL_Ref_No) {
      element.Status = incomingData?.status;
      element.CSL_Longitude_Time_In = incomingData?.longitude;
      element.CSL_Latitude_Time_In = incomingData?.latitude;
      element.CSL_Time_In_Date = incomingData?.timestamp;
      element.CSL_Location_Time_In = incomingData?.location;
      element.ArrivalDate = incomingData?.timestamp;
      element.arrived_Sync_Status = mode;
    }
  }



  // TODO: Syncing Offline Itineraries

  syncOfflineData() {
    this.NetworkStatus
      ? this.checkOfflineItineraries()
      : this.workschedService.presentOfflineSyncAlert();
  }


  checkOfflineItineraries() {
    this.storage.get(DeviceStorage.ItineraryList)
      .then((res: Result) => {

        let offlineItinerary: any = res.data.filter((element: any) => {
          return element.completed_Sync_status === 1;
        });

        this.workschedService.showLoading();

        this.setForSyncData(offlineItinerary);

      });
  }

  setForSyncData(incomingData: any) {

    incomingData.forEach((element: any) => {
      let dataUpdated = [{
        CSL_Result: element?.CSL_Result,
        CSL_Result_Date: element?.CSL_Result_Date,
        Latitude: element?.CSL_Latitude_Time_Out,
        Longitude: element?.CSL_Longitude_Time_Out,
        Location: element?.Location,
        CSL_Status_To_Remarks: element?.CSL_Status_To_Remarks,
        CSL_Result_Remarks: element?.CSL_Result_Remarks,
        ConfirmationDate: element?.ConfirmationDate,
        CSL_Latitude_Start: element?.CSL_Latitude_Start,
        CSL_Longitude_Start: element?.CSL_Longitude_Start,
        CSL_Location_Time_Start: element?.CSL_Location_Time_Start,
        ArrivalDate: element?.ArrivalDate,
        CSL_Latitude_Time_In: element?.CSL_Latitude_Time_In,
        CSL_Longitude_Time_In: element?.CSL_Longitude_Time_In,
        CSL_Location_Time_In: element?.CSL_Location_Time_In,
        CSL_Ref_No: element?.CSL_Ref_No,
        Attachment: element?.Attachment,
        CLI_Code: (element?.CLI_Code !== null ? element?.CLI_Code : this.branchCode),
        GeofenceClientStatus: element?.GeofenceStatus,
        GeofenceReason: element?.GeofenceReason,
      }];

      this.insertOfflineData(dataUpdated);
    });
  }


  insertOfflineData(forSyncingData: any) {
    this.workschedService
      .syncOfflineItinerary(forSyncingData, this.token)
      .subscribe({
        next: (res: any) => {
          if (res.statusCode !== "2000") {
            this.errorCtr++;
          }
          this.successCtr++;
          this.updateOfflineItineraryStorage(forSyncingData);

        },
        error: (error: any) => {
          this.workschedService
            .presentAlertError("Syncing Offline Data", error.message);
        },
      });
  }


  updateOfflineItineraryStorage(incomingData: any) {

    this.storage.get(DeviceStorage.ItineraryList)
      .then((res: any) => {

        res.data.forEach((storedData: any) => {
          if (incomingData[0].CSL_Ref_No === storedData?.CSL_Ref_No) {
            storedData.started_Sync_Status = 0;
            storedData.arrived_Sync_Status = 0;
            storedData.completed_Sync_status = 0;
          }
        });

        this.loadDWAOffline = res.data;
        this.storage.set(DeviceStorage.ItineraryList, res);
      });

    let total = this.successCtr + this.errorCtr;
    if (total >= this.offlineData) {
      this.presentAlertSyncSuccess(total);
    }
  }


  doRefresh(event: any) {
    this.loaded = false;
    setTimeout(() => {
      this.checkLocationPermission();
      this.getNetworkStatus();
      event.target.complete();
    }, 2000);
  }


  RefreshItineraryStorage() {
    this.NetworkStatus
      ? this.presentRemoveStorageAlert()
      : this.workschedService.presentOfflineCheckPermissionAlert();
  }

  checkRemovalPermission() {
    this.workschedService
      .checkRemovalPermission(this.userId, this.token)
      .subscribe({
        next: (res: Result) => {
          // if (res?.statusCode !== "2000") {
          //   this.workschedService
          //     .presentAlertError("No Permission!", "You are not authorized to make changes to your itinerary storage. To gain access, please contact the IT Department.");
          //   return;
          // }

          this.presentRemoveStorageAlert();
        },
        error: (error: Error) => {
          this.workschedService
            .presentAlertError("No Permission!", error.message);
        },
        complete: () => { }
      })
  }

  async presentAlertSyncSuccess(ctr: number) {
    Swal.fire({
      title: 'Success!',
      text: 'Sync ' + this.successCtr + ' of ' + ctr + ' Offline Itinerary of Client!',
      icon: 'success',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
    }).then((result) => {
      if (result.isConfirmed) {
        this.successCtr = 0;
        this.errorCtr = 0;
      }
    });

  }

  async presentRemoveStorageAlert() {
    Swal.fire({
      title: 'Update itinerary List?',
      text: 'All offline information will be deleted. Contact your IT for permission before proceeding.',
      icon: 'warning',
      heightAuto: false,
      allowOutsideClick: false,
      showCancelButton: true,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Continue',
    }).then((result) => {
      if (result.isConfirmed) {
        this.presentRemovalSuccessAlert();
      }
    });
  }

  presentRemovalSuccessAlert() {
    Swal.fire({
      title: 'Success!',
      text: 'Itinerary Storage has been updated',
      icon: 'success',
      heightAuto: false,
      allowOutsideClick: false,
      showCancelButton: true,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Continue',
    }).then((result) => {
      if (result.isConfirmed) {
        this.loaded = false;
        this.storage.remove(DeviceStorage?.ItineraryList);
        this.getNetworkStatus();
      }
    });
  }
}
