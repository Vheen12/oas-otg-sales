import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WorkschedulePageRoutingModule } from './workschedule-routing.module';

import { WorkschedulePage } from './workschedule.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WorkschedulePageRoutingModule
  ],
  declarations: [WorkschedulePage]
})
export class WorkschedulePageModule {}
