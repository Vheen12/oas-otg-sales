import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WorkschedulePage } from './workschedule.page';

const routes: Routes = [
  {
    path: '',
    component: WorkschedulePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorkschedulePageRoutingModule {}
