import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage-angular';
import { Device } from '@capacitor/device';
import { App } from '@capacitor/app';
import { Subscription } from 'rxjs';
import { DeviceStorage } from 'src/global';

import { AuthenticationService } from '../../services/authentication/authentication.service';
import { GeolocationService } from 'src/app/services/geolocation/geolocation.service';
import { NetworkConnectionService } from 'src/app/services/network-connection/network-connection.service';
import { ToastController } from '@ionic/angular';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit, OnDestroy {

  loginForm: FormGroup;
  appVersion: string = "";

  username: string = "";
  password: string = "";
  url: string = "";
  showPassword: boolean = false;

  private subscription = new Subscription();

  constructor(
    private router: Router,
    public authService: AuthenticationService,
    private storage: Storage,
    private activeroute: ActivatedRoute,
    private formBuilder: FormBuilder,
    public geolocationservice: GeolocationService,
    private network: NetworkConnectionService,
    private toastController: ToastController
  ) {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      deviceID: [''],
      latitude: [''],
      longitude: [''],
      userLocation: [''],
      version: [''],
    });
  }


  ngOnInit() {
    this.checkDevicePermission();
    this.getAppVersion();
    this.checkUserSession();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  ionViewWillEnter() {
    this.checkUserSession();
  }

  checkDevicePermission() {
    this.checkLocationPermission();
  }

  async getAppVersion() {
    try {
      this.appVersion = (await App.getInfo()).version + '.' + (await App.getInfo()).build;
    } catch (error) {
      this.appVersion = '2.0.2';
    }
  }


  async checkUserSession() {
    this.url = this.activeroute.snapshot.queryParamMap.get('returnto') || 'tabs';

    const IsAuthentication: any = localStorage.getItem('Authenticated');

    IsAuthentication === '0' ? this.userloggedIn() : this.userloggedOut();
  }


  private userloggedOut = () => {
    return this.router.navigateByUrl(this.url);
  }


  private userloggedIn = () => {
    return this.router.navigateByUrl(this.url, { replaceUrl: true });
  }


  Login() {
    this.network
      .checkNetworkConnection()
      .then((res: boolean) => {
        res
          ? this.showLoading()
          : this.presentError('There is no internet connection detected!');
      });
  }


  showLoading() {
    this.authService.showLoginLoading();
    this.insertLoginCredentials();
  }


  async insertLoginCredentials() {
    const deviceID = (await Device.getId()).identifier;

    await this.geolocationservice
      .getCoordinates()
      .then(async (res: any) => {
        await this.setLoginData(res, deviceID);
        this.saveLoginCredentials();
      })
      .catch((error: any) => {
        this.authService
          .presentAlertError('GPS Error on Device!', error.message);
      });
  }


  saveLoginCredentials() {
    let data: any;

    const LoginUser = this.authService
      .SignIn(this.loginForm.value)
      .subscribe({
        next: (res: any) => {
          data = res;
          this.authService.closeAlert();
        },
        error: (error: any) => {
          this.authService.presentAlertError("System Error ", error.message);
        },
        complete: () => {
          this.loginResult(data);
        }
      });
    this.subscription.add(LoginUser);
  }


  setLoginData = async (location: any, deviceID: string) => {
    this.loginForm.get('latitude')?.setValue(location.coords.latitude);
    this.loginForm.get('longitude')?.setValue(location.coords.longitude);
    this.loginForm.get('deviceID')?.setValue(deviceID);
    this.loginForm.get('version')?.setValue(this.appVersion);
  }


  loginResult(userData: any) {
    if (userData.statusCode !== '2000') {
      this.presentError(userData.message);
      return;
    }

    let access: string = (userData?.accessLevel);
    let access2: string = (userData?.accessLevel2);

    if (access === "ADAMIS-FMR" || access2 === "ADAMIS-FMR") {
      this.loginSuccess(userData, "FMR");
      return;
    }

    if (access === "ADAMIS-FMCM" || access2 === "ADAMIS-FMCM") {
      this.loginSuccess(userData, "FMR");
      return;
    }

    if (access === "ADAMIS-BAO" || access2 === "ADAMIS-BAO") {
      this.loginSuccess(userData, "BAO");
      return;
    }

    this.presentError('User Is Invalid! To update your system access, please contact the IT Department.');
  }


  loginSuccess = async (UserData: any, position: string) => {
    localStorage.setItem('Authenticated', '0');
    this.storage.set(DeviceStorage.UserInformation, UserData);
    this.storage.set(DeviceStorage.UserAccess, position);

    this.storage.get(DeviceStorage.AppInitialize)
      .then((res: boolean) => {

        !res
          ? this.router.navigateByUrl('/sync-mode')
          : this.router.navigateByUrl(this.url);
      });
  }


  presentError = async (message: string) => {
    this.authService.presentAlertError('Oops!', message);
    this.loginForm.reset();
  }


  checkLocationPermission = async () => {
    await this.geolocationservice
      .checkLocationPermission()
      .then((res: string) => {

        if (res !== "granted") {
          this.requestLocationPermission();
          return;
        }

      });
  }


  requestLocationPermission = async () => {
    this.geolocationservice.requestLocationPermission()
      .then(() => {

        return;

      }).catch((error: any) => {
        this.authService.presentAlertError('Permission Error!', error);
      });
  }
}
