import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TimekeepingPageRoutingModule } from './timekeeping-routing.module';

import { TimekeepingPage } from './timekeeping.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TimekeepingPageRoutingModule
  ],
  declarations: [TimekeepingPage]
})
export class TimekeepingPageModule {}
