import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TimekeepingPage } from './timekeeping.page';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('TimekeepingPage', () => {
  let component: TimekeepingPage;
  let fixture: ComponentFixture<TimekeepingPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TimekeepingPage],
      imports: [
        IonicModule.forRoot(),
        HttpClientTestingModule,
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(TimekeepingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
