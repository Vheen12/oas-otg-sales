import { Component, OnDestroy, OnInit } from '@angular/core';
import { TimekeepingService } from 'src/app/services/Timekeeping/timekeeping.service';
import { Storage } from '@ionic/storage-angular';
import { Router } from '@angular/router';
import { Network } from '@capacitor/network';
import { Subscription } from 'rxjs';
import Swal from 'sweetalert2';
import { DeviceStorage, URL_STRING } from 'src/global';
import { Browser } from '@capacitor/browser';
@Component({
  selector: 'app-timekeeping',
  templateUrl: './timekeeping.page.html',
  styleUrls: ['./timekeeping.page.scss'],
})
export class TimekeepingPage implements OnInit, OnDestroy {

  private subscription = new Subscription();

  public timeToday: any = new Date().toLocaleTimeString();
  public dateToday: any = new Date().toLocaleDateString('en-US', { dateStyle: "long" });

  protected token: string = "";

  HistoryAttendance: any = [];
  itineraryList: any = [];
  EmployeeName: any;
  loaded: boolean = false;
  disabled: boolean = false;
  announcementModal: boolean = false;
  timelogsModal: boolean = false;
  timeInOutText: string = 'Time In';
  attendance: any = [];

  timeIn: string = "";
  timeOut: string = "";

  constructor(
    public timekeepingService: TimekeepingService,
    private storage: Storage,
    private router: Router,
  ) {
    setInterval(() => {
      this.timeToday = new Date().toLocaleTimeString();
    }, 1);
  }

  async ngOnInit() {
    await this.checkNetworkStatus();
  }


  async ngOnDestroy() {
    this.subscription.unsubscribe();
  }


  async checkNetworkStatus() {
    (await Network.getStatus()).connected
      ? this.getUserInfo() : this.presentOfflineAlert();
  }

  async validateAttendance() {
    this.storage.get(DeviceStorage.ItineraryList).then((res: any) => {
      if (res) {
        this.itineraryList = res.data;
        this.checkOfflineData();
        return;
      }

      this.timeInOut()
    }).catch((error: any) => {
      console.error(error);
    });
  }


  async checkOfflineData() {

    let checkOfflineItinerary: boolean = this.itineraryList.some((element: any) => {
      return element.completed_Sync_status === 1;
    });

    if (checkOfflineItinerary) {
      this.timekeepingService.presentPendingAlert();
      this.timekeepingService.presentTextToSpeech('Please Sync Pending itinerary from offline storage to continue.');
      return;
    }

    this.timeInOutText === 'Time In'
      ? this.timeInOut() : this.verifyTimeOut();
  }


  async verifyTimeIn() {
    let checkPendingItinerary: boolean = this.itineraryList.every((element: any) => {
      return element.Status === "Pending";
    });

    if (checkPendingItinerary) {
      this.timekeepingService.presentNoStartedItineraryAlert();
      this.timekeepingService.presentTextToSpeech('There is no Itinerary Started. Please start an Itinerary first to time in.');
      return;
    }

    this.timeInOut();

  }

  async verifyTimeOut() {

    let checkCompletedItinerary: boolean = this.itineraryList.every((element: any) => {
      return element.Status === "Completed";
    });

    if (!checkCompletedItinerary) {
      this.timekeepingService.presentPendingItineraryAlert();
      this.timekeepingService.presentTextToSpeech('There are pending itineraries! Please Complete first to Time Out!');
      return;
    }

    this.timeInOut();

  }


  async getUserInfo() {
    this.storage.get(DeviceStorage.UserInformation).then((res: any) => {
      if (res) {
        this.EmployeeName = res.employeeName;
        this.token = res.token;
        this.loadAttendance(res.userId, res.token);
        this.checkUserAttendance(res.userId, res.token);
        this.loaded = true;
      }
    });
  }

  async loadAttendance(userID: string, token: string) {
    const getAttendance = this.timekeepingService.getAttendanceData(userID, token).subscribe({
      next: async (data: any) => {
        if (data.statusCode !== '2000') {
          this.timekeepingService.presentAlertError("Timekeeping: Fetching Data", data.message);
          return;
        }
        localStorage.setItem('attendance', JSON.stringify(data));
        await this.loadAttendanceData(data);
      },
      error: async (error: any) => {
        this.timekeepingService.presentAlertError("Timekeeping: Error Fetching Data", error.message);
      }
    });

    this.subscription.add(getAttendance);
  }

  async loadAttendanceData(attendance: any) {
    this.HistoryAttendance = attendance.data;
  }

  async checkUserAttendance(userID: string, token: string) {
    this.timekeepingService.getAttendanceToday(userID, token).subscribe({
      next: (res: any) => {
        if (res.statusCode !== '2000') {
          this.timeInOutText = 'Time In';
          this.timeIn = this.timeOut = undefined;
          return;
        }

        localStorage.setItem("timeInStatus", JSON.stringify(0));
        this.setAttendanceValue(res);

        res.data[0].Time_Out === null
          ? this.timeInOutText = 'Time Out'
          : this.timeInOutText = 'Completed Attendance';
      },
      error: (error: any) => {
        this.timekeepingService.presentAlertError("Timekeeping: Checking Attendance", error.message);
      },
      complete: () => { }
    })
  }

  private setAttendanceValue(res: any) {
    this.timeIn = res.data[0].Time_In?.date;
    this.timeOut = res.data[0].Time_Out?.date;
  }

  async timeInOut() {

    let ArrayAttendance: any;

    try {
      const AttendanceData: any = JSON.parse(localStorage.getItem('attendance'));

      AttendanceData.data.forEach((attendance: any) => {

        const dateToday = (attendance.TMK_Entry_Date === null) ? '' : new Date(attendance.TMK_Entry_Date.date).toLocaleDateString();
        const timeIn = ((attendance.TMK_Time_In === null) ? '' : new Date(attendance.TMK_Time_In.date).toLocaleTimeString());
        const timeOut = ((attendance.TMK_Time_Out === null) ? '' : new Date(attendance.TMK_Time_Out.date).toLocaleTimeString());

        if (dateToday === this.dateToday) {

          ArrayAttendance = {

            dateToday,
            timeIn,
            timeOut
          };

        }
      });

      const undefinedResult = {

        dateToday: this.dateToday,
        timeIn: '',
        timeOut: ''
      };

      const arrayAttendanceCheck = ((ArrayAttendance === undefined) ? undefinedResult : ArrayAttendance);

      localStorage.setItem('attendance_data', JSON.stringify(arrayAttendanceCheck));
      this.router.navigateByUrl('/timekeeping-time-out');

    } catch (error) {


      const undefinedResult = {

        dateToday: this.dateToday,
        timeIn: '',
        timeOut: ''
      };

      const arrayAttendanceCheck = ((ArrayAttendance === undefined) ? undefinedResult : ArrayAttendance);

      localStorage.setItem('attendance_data', JSON.stringify(arrayAttendanceCheck));
      this.router.navigateByUrl('/timekeeping-time-out');

    }

  }

  async doRefresh(event: any) {
    this.loaded = false;
    const NetworkStatus = await Network.getStatus();

    setTimeout(() => {
      NetworkStatus.connected ? this.getUserInfo() : this.checkNetworkStatus();
      event.target.complete();
    }, 1500);
  }


  async presentOfflineAlert() {
    Swal.fire({
      title: 'No internet connection detected!',
      text: 'Please connect to the internet to time in.!',
      icon: 'error',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonText: 'Retry',
      confirmButtonColor: '#0e8140',
      showCancelButton: true,
      cancelButtonText: 'Back To Dashboard'
    }).then((result) => {
      if (result.isConfirmed) {
        this.checkNetworkStatus();
      } else {
        this.router.navigate(["tabs/"]);
      }
    });
  }


  openModal(isOpen: boolean, modal: string) {
    if (modal === 'announcement') {
      this.announcementModal = isOpen;
      return;
    }

    this.timelogsModal = isOpen;
  }


  async proceedActivity() {
    this.announcementModal = false;
    this.timekeepingService.showSurveyRedirecting();
    setTimeout(async () => {
      this.timekeepingService.closeAlert();
      await Browser.open({ url: `${URL_STRING.BASE_URL}/otgLogin/?method=HRISV2/EES_Survey&token=${this.token}` });
    }, 1500);
  }


}
