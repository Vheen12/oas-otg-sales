import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TimekeepingPage } from './timekeeping.page';

const routes: Routes = [
  {
    path: '',
    component: TimekeepingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TimekeepingPageRoutingModule {}
