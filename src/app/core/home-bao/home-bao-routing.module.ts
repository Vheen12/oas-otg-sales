import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeBaoPage } from './home-bao.page';

const routes: Routes = [
  {
    path: '',
    component: HomeBaoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeBaoPageRoutingModule {}
