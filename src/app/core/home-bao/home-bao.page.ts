import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { DeviceStorage } from 'src/global';
import { format } from 'date-fns';
import { TrListService } from 'src/app/services/tr-list/tr-list.service';
import { NetworkConnectionService } from 'src/app/services/network-connection/network-connection.service';
import { HomeService } from 'src/app/services/home/home.service';

export interface UserData {
  employeeName?: string;
  position?: string;
  userId?: string;
  token?: string;
  tokenDate?: string;
}

export interface Result {
  data?: any[];
  message?: string;
  statusCode?: string;
}

@Component({
  selector: 'app-home-bao',
  templateUrl: './home-bao.page.html',
  styleUrls: ['./home-bao.page.scss'],
})
export class HomeBaoPage implements OnInit {

  showAddClient: boolean = false;
  userData: UserData;


  loaded: boolean = false;
  total: number = 0;
  pending: number = 0;
  received: number = 0;

  dateToday = format(new Date(), "yyyy-MM-dd");

  constructor(
    private storage: Storage,
    private trservice: TrListService,
    private network: NetworkConnectionService,
    private homeService: HomeService,
  ) { }


  ngOnInit() {
    this.resetVariables();
    this.getUserInfo();
  }

  ionViewWillEnter() {
    this.loaded = false;
    this.resetVariables();
    this.getUserInfo();
  }


  checkTokenAccess(data: any) {
    if (data.tokenDate !== this.dateToday) {
      this.homeService.presentTokenExpiredAlert();
      return;
    }
    this.checkNetworkStatus(data);
  }


  async getUserInfo() {
    await this.storage.get(DeviceStorage.UserInformation)
      .then((res: UserData) => {
        this.userData = res;
        this.checkTokenAccess(res);
      });
  }

  checkNetworkStatus(data: any) {
    this.network.checkNetworkConnection()
      .then((res: boolean) => {

        res && this.getStackTransferList(data.token, data.branchCode, data.userId);

      });
  }

  getStackTransferList(token: string, branch: string, userID: string) {
    this.trservice.getTransferReceiptList(token)
      .subscribe({
        next: (res: any) => {

          this.filterList(res, branch, userID);

        },
        error: (error: Error) => {
          this.trservice
            .presentAlertError("Oops!", error.message);
          this.loaded = true;

        },
        complete: () => {
          this.loaded = true;
        },
      })
  }

  async filterList(res: any, branch: string, userID: string) {
    console.log(res);

    if (res.statusCode !== "2000") {
      return;
    }

    if (userID === '210000997') {
      this.total = res.data.length;
      return;
    }

    this.total = res.data.filter((element: any) => {
      return element.Branch === branch;
    }).length;

  }


  doRefresh(event: any) {
    this.loaded = false;
    this.resetVariables();

    setTimeout(() => {
      this.getUserInfo();
      event.detail.complete();
    }, 3000);
  }


  resetVariables() {
    this.total = 0;
    this.pending = 0;
    this.received = 0;
  }

}
