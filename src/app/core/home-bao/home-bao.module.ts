import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeBaoPageRoutingModule } from './home-bao-routing.module';

import { HomeBaoPage } from './home-bao.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeBaoPageRoutingModule
  ],
  declarations: [HomeBaoPage]
})
export class HomeBaoPageModule {}
