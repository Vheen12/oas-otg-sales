import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HomeBaoPage } from './home-bao.page';

describe('HomeBaoPage', () => {
  let component: HomeBaoPage;
  let fixture: ComponentFixture<HomeBaoPage>;

  beforeEach(waitForAsync () => {
    fixture = TestBed.createComponent(HomeBaoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
