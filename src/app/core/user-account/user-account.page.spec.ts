import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { UserAccountPage } from './user-account.page';
import { Storage } from '@ionic/storage';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('UserAccountPage', () => {
  let component: UserAccountPage;
  let fixture: ComponentFixture<UserAccountPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [UserAccountPage],
      imports: [
        HttpClientTestingModule,
      ],
      providers: [Storage]
    }).compileComponents();

    fixture = TestBed.createComponent(UserAccountPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
