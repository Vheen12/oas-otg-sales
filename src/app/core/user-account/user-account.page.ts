import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { App } from '@capacitor/app';
import { Device } from '@capacitor/device';
import { NetworkConnectionService } from 'src/app/services/network-connection/network-connection.service';
import { UserAccountService } from 'src/app/services/user-account/user-account.service';
import { Storage } from '@ionic/storage';
import { DeviceStorage } from 'src/global';
@Component({
  selector: 'app-user-account',
  templateUrl: './user-account.page.html',
  styleUrls: ['./user-account.page.scss'],
})
export class UserAccountPage implements OnInit {
  appVersion: string = "";
  token: string = "";

  public appMenu = [
    // { id: 0, label: 'Add Client', icon: 'person-add' },
    // { id: 1, label: 'Create PAIF', icon: 'create' },
    { id: 1, label: 'Sync Data', icon: 'sync' },
    // { id: 3, label: 'Rate App', icon: 'star' },
    // { id: 4, label: 'Guide', icon: 'Information-circle' },
    // { id: 5, label: 'Help', icon: 'help-circle' },
    { id: 2, label: 'Check Update', icon: 'cloud-upload' },
    { id: 3, label: 'Permissions', icon: 'lock-closed' },
    { id: 4, label: 'Log Out', icon: 'log-out' },
  ];

  constructor(
    private storage: Storage,
    private router: Router,
    private network: NetworkConnectionService,
    private accountService: UserAccountService,
  ) {
    this.getAppVersion();
  }

  ngOnInit() {
    this.getUserInfo();
  }

  async getUserInfo() {
    this.storage.get(DeviceStorage.UserInformation).then((res: any) => {
      this.token = res?.token;
    })
  }

  async getAppVersion() {
    try {
      this.appVersion = (await App.getInfo()).version.toString() + '.' + (await App.getInfo()).build.toString();
    } catch (error) {
      this.appVersion = '2.0.2';
    }
  }

  async checkAppVersion(token: string) {
    try {
      const apkVersion = (await App.getInfo()).version + '.' + (await App.getInfo()).build;

      this.network.checkNetworkConnection().then((res: boolean) => {
        res ? this.fetchAppVersion(apkVersion, token) : this.checkStorageStatus();
      });

    } catch (error: any) {
      this.accountService.presentAlertError("Error Checking App Version", error.message);
    }
  }

  async fetchAppVersion(appVersion: any, token: string) {
    this.accountService.getAppVersion(token).subscribe({
      next: (res: any) => {
        this.compareAppVersion(res, appVersion);
      },
      error: (error: any) => {
        this.accountService.presentAlertError("Fetching App Version Error", error.message);
      },
    });
  }

  private compareAppVersion(res: any, apkVersion: string) {
    if (res.statusCode !== '2000') {
      this.accountService.presentAlertError("Fetch App Version", res.message);
      return;
    }

    const appInfo = res.data[0];

    if (appInfo.AppVersion > apkVersion) {
      this.accountService.presentNewUpdateAlert(appInfo.AppVersion);
      return;
    }

    this.accountService.showDeviceUpdateAlert();

  }


  doTask(id: number) {
    switch (id) {
      case 0:
        this.addClient();
        break;
      case 1:
        this.syncAppData();
        break;
      case 2:
        this.showDeviceUpdate();
        break;
      case 3:
        this.goToDevicePermission();
        break;
      case 4:
        this.goToLogout();
        break;

      default:
        break;
    }
  }


  async checkStorageStatus() {
    const deviceStorage = (await Device.getInfo()).realDiskFree;
    if (deviceStorage <= 4294967296) {
      this.accountService.presentStorageFullAlert();
    }
  }

  async showDeviceUpdate() {
    this.accountService.presentCheckUpdateLoader('Checking for Update...');
    this.checkAppVersion(this.token);
  }

  async syncAppData() {
    this.network.checkNetworkConnection().then((res: boolean) => {
      res ? this.goToSyncMode()
        : this.accountService.presentAlertError('You are in Offline Mode!', 'Please Connect to the Internet to Sync!');
    });
  }

  async addClient() {
    this.accountService.addClient();
  }

  async goToLogout() {
    this.accountService.presentLogoutAlert();
  }

  async goToSyncMode() {
    this.accountService.presentSyncAppAlert();
  }

  async goToHelpAndSupport() {
    this.router.navigateByUrl("faqs");
  }

  async goToRatingPage() {
    this.router.navigateByUrl("rating");
  }

  async goToDevicePermission(){
    this.router.navigateByUrl("permission");
  }
}
