import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage-angular';
import { Subscription } from 'rxjs';
import Swal from 'sweetalert2';
import { DeviceStorage } from 'src/global';
import { Router } from '@angular/router';
import { StorageDataService } from 'src/app/services/storage-data/storage-data.service';
import { ProspectService } from 'src/app/services/prospect/prospect.service';
StorageDataService
@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.page.html',
  styleUrls: ['./add-client.page.scss'],
})
export class AddClientPage implements OnInit {
  @ViewChild('swiper')
  swiperRef: ElementRef | undefined;

  clientForm: FormGroup;

  sectionNumber: number = 1;
  progress: number = 0;

  proceedToNextSlide() {
    this.swiperRef?.nativeElement.swiper.slideNext();
    this.progress = parseFloat((this.swiperRef?.nativeElement.swiper.progress));
    this.sectionNumber = this.swiperRef?.nativeElement.swiper.activeIndex + 1;
  }

  proceedToPrevSlide() {
    this.swiperRef?.nativeElement.swiper.slidePrev();
    this.progress = parseFloat((this.swiperRef?.nativeElement.swiper.progress));
    this.sectionNumber = this.swiperRef?.nativeElement.swiper.activeIndex - 1;
  }


  private subscription = new Subscription();

  isModalOpen = false;

  presentAddressSection = false;
  corporateInputSection = false;

  userID: any;
  branch: any;
  token: any;
  tipster: any = '';

  provinceList: any = [];
  cityList: any = [];
  barangayList: any = [];
  activityList: any = [];
  sourceList: any = [];
  agentList: any = [];
  occupationList: any = [];
  productList: any = [];
  modelList: any = [];
  corporateList: any = [];

  contactType = 'tel';
  contactLength = 11;
  contactPattern = '[0-9]{11}';
  contactPlaceholder = 'e.g. 09057865332';

  contactForm = [
    {
      label: 'Mobile',
      type: 'tel',
      pattern: '[0-9]{11}',
      maxlength: 11,
      placeholder: 'e.g. 09057865332',
    },
    {
      label: 'Home',
      type: 'tel',
      pattern: '[0-9]{3}-[0-9]{2}-[0-9]{3}',
      maxlength: 10,
      placeholder: 'e.g. 090-57-865',
    },
    {
      label: 'Email',
      type: 'email',
      pattern: '',
      maxlength: 35,
      placeholder: 'e.g. john@mail.com',
    },
    {
      label: 'Fax',
      type: 'text',
      pattern: '',
      maxlength: 35,
      placeholder: '',
    },
    {
      label: 'Social Media Link',
      type: 'text',
      pattern: '',
      maxlength: 35,
      placeholder: '',
    }
  ];

  constructor(
    private formBuilder: FormBuilder,
    private storage: Storage,
    private prospectService: ProspectService,
    private router: Router,
    private storedData: StorageDataService,
  ) {
    this.initializeForm();
  }

  initializeForm() {
    this.clientForm = this.formBuilder.group({
      ClientSource: ['', [Validators.required]],
      BranchCode: [''],
      LastName: ['', [Validators.required, Validators.maxLength(30)]],
      FirstName: ['', [Validators.required, Validators.maxLength(30)]],
      MiddleName: ['', [Validators.required, Validators.maxLength(30)]],
      FarmLand: [''],
      Activity: ['', [Validators.required]],
      SalesmanId: [''],
      PostingDate: [''],
      LeadType: ['Individual', [Validators.required]],
      Company: [''],
      CorporateAccountType: [''],
      AffiliatedOrganization: [''],
      BirthDate: [''],
      RepeatBuyer: ['NO', [Validators.required]],
      OtherBrand: ['NO', [Validators.required]],
      Tipster: new FormControl({ value: '', disabled: true }),
      ActivityDate: ['', [Validators.required]],
      HomeProvince: ['', [Validators.required]],
      HomeCity: ['', [Validators.required]],
      HomeBrgy: ['', [Validators.required]],
      HomeStreet: ['', [Validators.required]],
      HomeLandmark: [''],
      PresentProvince: ['', [Validators.required]],
      PresentCity: ['', [Validators.required]],
      PresentBrgy: ['', [Validators.required]],
      PresentStreet: ['', [Validators.required]],
      PresentLandmark: [''],
      Occupation: ['', [Validators.required]],
      ContactDetails: this.formBuilder.array([
        this.contactFormGroup()
      ]),
      ProductOfInterest: this.formBuilder.array([
        this.productFormGroup()
      ]),
    });
  }

  contactFormGroup(): FormGroup {
    return this.formBuilder.group({
      ContactType: ['Mobile', [Validators.required]],
      Particulars: ['', Validators.required]
    });
  }

  contactQuantities(): FormArray {
    return this.clientForm.get('ContactDetails') as FormArray;
  }

  addContactField() {
    this.contactQuantities().push(this.contactFormGroup());
  }

  removeContactField(i: number) {
    this.contactQuantities().removeAt(i);
  }

  productFormGroup(): FormGroup {
    return this.formBuilder.group({
      Model: ['', [Validators.required]],
      TxtTargetPurchaseDate: [''],
    });
  }

  productQuantities(): FormArray {
    return this.clientForm.get('ProductOfInterest') as FormArray;
  }

  addProductField() {
    this.productQuantities().push(this.productFormGroup());
  }

  removeProductField(i: number) {
    this.productQuantities().removeAt(i);
  }


  ngOnInit() {
    this.getStoredData();
  }


  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


  getStoredData() {
    this.provinceList = this.storedData.getProvinceList();
    this.cityList = this.storedData.getCityList();
    this.barangayList = this.storedData.getBarangayList();
    this.activityList = this.storedData.getActivityList();
    this.sourceList = this.storedData.getClientSourceList();
    this.agentList = this.storedData.getAgentList();
    this.occupationList = this.storedData.getOccupationList();
    this.corporateList = this.storedData.getCorporateList();
    this.productList = this.storedData.getProductList();
    this.modelList = this.storedData.getModelList();
  }

  saveClient() {
    console.log(this.clientForm.value);
  }

  verifyClientName() {
    let firstName: string = this.clientForm.get("FirstName")?.value;
    let middleName: string = this.clientForm.get("MiddleName")?.value;
    let lastName: string = this.clientForm.get("LastName")?.value;

    if (firstName !== "" && middleName !== "" && lastName !== "") {
      this.prospectService.presentClientVerification();
      setTimeout(() => {
        console.log("done");
        Swal.close();
      }, 3000);
      return;
    }

    this.prospectService.presentAlertError("Empty Fields","Please fill in the fields for Last Name, First Name and Middle Name to Verify!");
  }
  // segmentChanged(event: any) {
  //   event.detail.value === 'Corporate' ? this.corporateInputSection = true : this.corporateInputSection = false;
  //   this.prospectForm.get('LeadType')?.setValue(event.detail.value);
  // }

  // private setPostingDate() {
  //   const datenow = new Date().toLocaleDateString();
  //   this.prospectForm.get('PostingDate').setValue(datenow);
  // }

  // showPresentAddress(event: any) {
  //   if (event.detail.checked) {
  //     this.presentAddressSection = true;
  //     this.prospectForm.get('PresentProvince').setValue(this.homeprovince.value);
  //     this.prospectForm.get('PresentCity').setValue(this.homecity.value);
  //     this.prospectForm.get('PresentBrgy').setValue(this.homebrgy.value);
  //     this.prospectForm.get('PresentStreet').setValue(this.homestreet.value);
  //   } else {
  //     this.presentAddressSection = false;
  //     this.prospectForm.get('PresentProvince').setValue('');
  //     this.prospectForm.get('PresentCity').setValue('');
  //     this.prospectForm.get('PresentBrgy').setValue('');
  //     this.prospectForm.get('PresentStreet').setValue('');
  //   }
  // }

  // async capitalizeFormValues(obj) {
  //   for (const key in obj) {
  //     if (obj[key] !== null && typeof obj[key] === 'object') {
  //       this.capitalizeFormValues(obj[key]);
  //     } else if (obj[key] !== null && typeof obj[key] === 'string') {
  //       obj[key] = obj[key].toUpperCase();
  //     }
  //   }
  // }

  // async savedClientInfo() {
  //   this.setFormValue();
  //   this.enableTipster();

  //   const arrData: any = [];
  //   this.capitalizeFormValues(this.prospectForm.value);
  //   arrData.push(this.prospectForm.value);

  //   const insertClient = this.prospectClientService.insertOPIS_ClientInformation(arrData, this.token).subscribe(
  //     {
  //       next: (res: any) => {
  //         if (res.statusCode !== '2000') {
  //           this.alerts.presentAlertError("Prospect Form: Saving Client Info", res.message);
  //           return;
  //         }

  //         this.presentLoader();
  //       },
  //       error: (error: any) => {
  //         this.alerts.presentAlertError("Prospect Form: Saving Client Info", error.message);

  //       }
  //     }
  //   );

  //   this.subscription.add(insertClient);

  // }

  // // setOpen(isOpen: boolean) {
  // //   this.isModalOpen = isOpen;
  // //   this.isModalOpen && this.getClientHistory();
  // // }

  // // setTipsterValue(event: any) {
  // //   const source = event.detail.value;

  // //   this.leadSourceListArray.forEach((element: any) => {
  // //     if (element.Code === source) {
  // //       element.Category === 'Referral' ? this.enableTipster() : this.disableTipster();
  // //       return;
  // //     }
  // //   });
  // // }

  // // getTipsterValue(event: any) {
  // //   this.tipster = event.detail.value
  // // }

  // // enableTipster() {
  // //   this.prospectForm.get('Tipster')?.enable();
  // // }

  // // disableTipster() {
  // //   this.prospectForm.get('Tipster')?.disable();
  // // }

  // // private setFormValue() {
  // //   this.prospectForm.get('BranchCode').setValue(this.branch);
  // //   this.prospectForm.get('SalesmanId').setValue(this.userID);
  // //   this.prospectForm.get('Tipster').setValue(this.tipster);
  // // }

  // private storeData() {
  //   // this.getUserInfo();
  //   // this.getHomeProvinceList();
  //   // this.getPresentProvinceList();
  //   // this.getActivityList();
  //   // this.getLeadSourceList();
  //   // this.getOccupationList();
  //   // this.getAgentList();
  //   // this.getProductList();
  //   // this.getCorporateAccountList();

  //   this.provinceList = this.storedData.getProvinceList();

  // }

  // changeInputType(type: any) {

  //   for (let i = 0; i < this.contactForm.length; i++) {
  //     const element = this.contactForm[i];


  //     if (element.label === type.detail.value) {
  //       this.contactType = element.type;
  //       this.contactLength = element.maxlength;
  //       this.contactPattern = element.pattern;
  //       this.contactPlaceholder = element.placeholder;

  //       break;
  //     }
  //   }
  // }

  // //#region Get Prospect Form Data
  // get homeprovince() {
  //   return this.prospectForm.get('HomeProvince');
  // }

  // get homecity() {
  //   return this.prospectForm.get('HomeCity');
  // }

  // get homebrgy() {
  //   return this.prospectForm.get('HomeBrgy');
  // }

  // get homestreet() {
  //   return this.prospectForm.get('HomeStreet');
  // }

  // get presentprovince() {
  //   return this.prospectForm.get('PresentProvince');
  // }

  // get presentcity() {
  //   return this.prospectForm.get('PresentCity');
  // }

  // get presentbrgy() {
  //   return this.prospectForm.get('PresentBrgy');
  // }

  // get activityValue() {
  //   return this.prospectForm.get('Activity');
  // }

  // get clientSource() {
  //   return this.prospectForm.get('ClientSource');
  // }

  // get agent() {
  //   return this.prospectForm.get('Tipster');
  // }

  // get occupation() {
  //   return this.prospectForm.get('Occupation');
  // }

  // get corporateAccountType() {
  //   return this.prospectForm.get('CorporateAccountType');
  // }


  // getToggleValue(event: any, formName: any) {
  //   event.detail.checked ? this.prospectForm.get(formName).setValue('YES') : this.prospectForm.get(formName).setValue('NO');
  // }
  // //#endregion

  // //#region Fetch Data

  // async getUserInfo() {
  //   await this.storage.get(DeviceStorage.UserInformation)
  //     .then((userInfo) => {
  //       this.branch = userInfo.branchCode;
  //       this.userID = userInfo.userId;
  //       this.token = userInfo.token;
  //     }).catch((err) => {
  //       console.log(err);
  //     });
  // }

  // // getHomeProvinceList() {
  // //   this.storage.get(DeviceStorage.ProvinceList).then((result) => {
  // //     this.homeprovinceListArray = [];
  // //     result.data.forEach((data: any) => this.homeprovinceListArray.push(data));
  // //   }).catch((err) => {
  // //     console.log(err);
  // //   });
  // // }

  // // getHomeCityList(province: any) {
  // //   const input = province.detail.value;
  // //   this.storage.get(DeviceStorage.CityList).then((result) => {
  // //     this.homecityListArray = [];
  // //     this.homebarangayListArray = [];
  // //     result.data.forEach((data: any) => {
  // //       if (data.stateID === input) { this.homecityListArray.push(data); }
  // //     });
  // //   }).catch((err) => {
  // //     console.log(err);
  // //   });
  // // }

  // // getHomeBarangayList(city: any) {
  // //   const input = city.detail.value;
  // //   this.storage.get(DeviceStorage.BarangayList).then((result) => {
  // //     this.homebarangayListArray = [];
  // //     result.data.forEach((data: any) => {
  // //       if (data.cityID === input) { this.homebarangayListArray.push(data); }
  // //     });
  // //   }).catch((err) => {
  // //     console.log(err);
  // //   });
  // // }

  // // getPresentProvinceList() {
  // //   this.storage.get(DeviceStorage.ProvinceList).then((result) => {
  // //     this.presentprovinceListArray = [];
  // //     result.data.forEach((data: any) => this.presentprovinceListArray.push(data));
  // //   }).catch((err) => {
  // //     console.log(err);
  // //   });
  // // }

  // // getPresentCityList(province: any) {
  // //   const input = province.detail.value;
  // //   this.storage.get(DeviceStorage.CityList).then((result) => {
  // //     this.presentcityListArray = [];
  // //     result.data.forEach((data: any) => {
  // //       if (data.stateID === input) { this.presentcityListArray.push(data); }
  // //     });
  // //   }).catch((err) => {
  // //     console.log(err);
  // //   });
  // // }

  // // getPresentBarangayList(city: any) {
  // //   const input = city.detail.value;
  // //   this.storage.get(DeviceStorage.BarangayList).then((result) => {
  // //     this.presentbarangayListArray = [];
  // //     result.data.forEach((data: any) => {
  // //       if (data.cityID === input) { this.presentbarangayListArray.push(data); }
  // //     });
  // //   }).catch((err) => {
  // //     console.log(err);
  // //   });
  // // }

  // // async getActivityList() {
  // //   await this.storage.get(DeviceStorage.ActivityList).then((result) => {
  // //     this.activityListArray = [];
  // //     result.data.forEach((data: any) => this.activityListArray.push(data));
  // //   });
  // // }

  // // async getLeadSourceList() {
  // //   await this.storage.get(DeviceStorage.ClientSourceList).then((result) => {
  // //     this.leadSourceListArray = [];
  // //     result.data.forEach((data: any) => this.leadSourceListArray.push(data));
  // //   });
  // // }

  // // async getAgentList() {
  // //   await this.storage.get(DeviceStorage.AgentList).then((result) => {
  // //     this.agentListArray = [];
  // //     result.data.forEach((data: any) => {
  // //       if (data.Branch === this.branch) {
  // //         this.agentListArray.push(data);
  // //       }
  // //     });
  // //   });
  // // }

  // // async getOccupationList() {
  // //   await this.storage.get(DeviceStorage.OccupationList).then((result) => {
  // //     this.occupationListArray = [];
  // //     result.data.forEach((data: any) => this.occupationListArray.push(data));
  // //   });
  // // }

  // // async getProductList() {
  // //   await this.storage.get(DeviceStorage.ProductList).then((result) => {
  // //     this.productListArray = [];
  // //     result.data.forEach((data: any) => this.productListArray.push(data));
  // //   });
  // // }

  // // async getProductModelList(product: any) {
  // //   const input = product.detail.value;
  // //   await this.storage.get(DeviceStorage.ProductModelList).then((result) => {
  // //     this.productModelListArray = [];
  // //     result.data.forEach((data: any) => data.Class === input && this.productModelListArray.push(data));
  // //   });
  // // }

  // // async getCorporateAccountList() {
  // //   await this.storage.get(DeviceStorage.CorporateAccountList).then((result) => {
  // //     this.corporateAccountListArray = [];
  // //     result.data.forEach((data: any) => this.corporateAccountListArray.push(data));
  // //   });
  // // }

  // // async getClientHistory() {
  // //   await this.getUserInfo();
  // //   const getAddedClient = this.prospectClientService.getNewProspectProfile(this.userID, this.token)
  // //     .subscribe(
  // //       {
  // //         next: (res: any) => {
  // //           this.prospectAddedListArray = [];

  // //           if (res.statusCode !== '2000') {
  // //             this.alerts.presentAlertError("Prospect Form: Get Client History", res.message);
  // //             return;
  // //           }

  // //           res.data.forEach((row: any) => {
  // //             this.prospectAddedListArray.push(row);
  // //           });
  // //         },
  // //         error: (error: any) => {
  // //           this.alerts.presentAlertError("Prospect Form: Get Client History Error", error.message);
  // //         }
  // //       }
  // //     );
  // //   this.subscription.add(getAddedClient);
  // // }
  // //#endregion

  // //#region  Alerts and loaders
  // async presentSuccessSaveAlert() {
  //   Swal.fire({
  //     title: 'Success!',
  //     text: 'Successfully Saved!',
  //     icon: 'success',
  //     heightAuto: false,
  //     allowOutsideClick: false
  //   }).then((result) => {
  //     if (result.isConfirmed) {
  //       this.presentAddressSection = false;
  //       this.corporateInputSection = false;

  //       this.prospectForm.markAsPristine();
  //       this.prospectForm.reset();
  //       this.initializeForm();
  //       this.router.navigateByUrl('/home');
  //     }
  //   });
  // }

  // async presentLoader() {
  //   Swal.fire({
  //     title: 'Loading ...',
  //     heightAuto: false,
  //     allowOutsideClick: false,
  //     timer: 5000,
  //     didOpen: () => {
  //       Swal.showLoading();
  //     }
  //   }).then((result) => {
  //     if (result.dismiss) {
  //       this.presentSuccessSaveAlert();
  //     }
  //   });
  // }

  // async presentSavedPrompt() {
  //   Swal.fire({
  //     title: 'Saved Client Information?',
  //     icon: 'question',
  //     heightAuto: false,
  //     allowOutsideClick: false,
  //     showCancelButton: true,
  //     confirmButtonText: 'Save',
  //   }).then((result) => {
  //     if (result.isConfirmed) {
  //       this.savedClientInfo();
  //     }
  //   });
  // }

}
