import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.page.html',
  styleUrls: ['./faqs.page.scss'],
})
export class FaqsPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public items = [
    {
      ID: 1,
      icon: "../../../assets/img/interrogation.svg",
      title: "Getting Started",
      subtitle: "Learn the Basics of using the app.",
      link: "/pdf-viewer",
    },
    {
      ID: 2,
      icon: "../../../assets/img/sign-in-alt.svg",
      title: "Failure to Log In",
      subtitle: "When logging into the app, an error occurs.",
      link: "",
    },
    {
      ID: 3,
      icon: "../../../assets/img/compass-slash.svg",
      title: "GPS Not Working",
      subtitle: "Problems on  Device's GPS/Location.",
      link: "",
    },
    {
      ID: 4,
      icon: "../../../assets/img/briefcase.svg",
      title: "Itinerary Not Starting",
      subtitle: "When performing daily work schedule, there is no response.",
      link: "",
    },
  ];

}
