import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FaqsPage } from './faqs.page';

describe('FaqsPage', () => {
  let component: FaqsPage;
  let fixture: ComponentFixture<FaqsPage>;

  // beforeEach(waitForAsync () => {
  //   fixture = TestBed.createComponent(FaqsPage);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  // });

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FaqsPage ]
    }).compileComponents();

    fixture = TestBed.createComponent(FaqsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
