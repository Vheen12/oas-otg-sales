import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';
import * as CanvasJSAngularChart from 'src/assets/canvasjs/canvasjs.angular.component';
const CanvasJSChart = CanvasJSAngularChart.CanvasJSChart;

import { IonVideoPlayerModule } from 'ion-video-player';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    IonVideoPlayerModule

  ],
  declarations: [HomePage, CanvasJSChart],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class HomePageModule { }
