import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { DeviceStorage } from 'src/global';
import { format } from 'date-fns'
import { NetworkConnectionService } from 'src/app/services/network-connection/network-connection.service';
import { HomeService } from 'src/app/services/home/home.service';
export interface UserData {
  employeeName?: string;
  position?: string;
  userId?: string;
  token?: string;
  tokenDate?: string;
}

export interface Result {
  data?: any[];
  message?: string;
  statusCode?: string;
}

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  showAddClient: boolean = false;
  userData: UserData;
  appVersion: string = "";

  announcementUrl: string = "";
  basePath: string = "https://adamis.adamco.com.ph/";

  loaded: boolean = false;
  announcementModal: boolean = false;
  total: number = 0;
  arrived: number = 0;
  pending: number = 0;
  completed: number = 0;
  started: number = 0;

  dateToday = format(new Date(), "yyyy-MM-dd");

  constructor(
    private storage: Storage,
    private network: NetworkConnectionService,
    private homeService: HomeService,
  ) { }


  ngOnInit() {
    this.resetVariables();
    this.getUserInfo();
  }

  ionViewWillEnter() {
    this.loaded = false;
    this.resetVariables();
    this.getUserInfo();
  }

  showAddClientButton() {
    this.storage.get(DeviceStorage.UserAccess)
      .then((res: string) => {
        if (res === "FMR") {
          this.showAddClient = true;
        }
      })
  }

  checkTokenAccess(data: any) {
    if (data.tokenDate !== this.dateToday) {
      this.homeService.presentTokenExpiredAlert();
      return;
    }
    this.checkNetworkStatus(data);
  }


  async getUserInfo() {
    await this.storage.get(DeviceStorage.UserInformation)
      .then((res: UserData) => {
        this.userData = {
          employeeName: res?.employeeName,
          position: res?.position,
          userId: res?.userId,
          token: res?.token,
          tokenDate: res?.tokenDate,
        };

        this.checkTokenAccess(res);
      });
  }

  checkNetworkStatus(data: any) {
    this.network.checkNetworkConnection()
      .then((res: boolean) => {

        res
          ? this.runOnlineMode(data)
          : this.runOfflineMode();

      });
  }


  async runOnlineMode(data: any) {
    await this.fetchItinerary();
    await this.getAnnouncements(data?.token);
  }


  async runOfflineMode() {
    this.loadDashboardData();
    this.loaded = true
  }


  async getAnnouncements(token: string) {
    this.homeService.getAnnouncements(token)
      .subscribe({
        next: (res: Result) => {

          if (res.statusCode === '-1005') {
            return;
          }

          if (res.statusCode !== '2000') {
            this.homeService
              .presentAlertError('Announcement!', res.message);
            return;
          }

          this.checkAnnouncementStatus(res);

        },
        error: (err: Result) => {
          this.homeService
            .presentAlertError('Announcement Error!', err.message);
        },
      });
  }


  checkAnnouncementStatus(res: any) {
    let validityStart: any = new Date(res.data[0].Validity_Start?.date).toISOString();
    let validityEnd: any = new Date(res.data[0].Validity_End?.date).toISOString();
    let dateToday: any = new Date().toISOString();

    if ((dateToday >= validityStart && dateToday <= validityEnd)) {
      this.showAnnouncement(res);
      return;
    }
  }


  showAnnouncement(res: any) {
    let ads = localStorage.getItem('announcement');

    if (ads === null) {
      localStorage.setItem('announcement', '0');
      this.announcementUrl = `${this.basePath}${res.data[0].Url}`;
      this.showAnnouncementModal(true);
    }
  }


  async fetchItinerary() {
    this.homeService
      .getItinerary(this.userData?.userId, this.userData?.token)
      .subscribe({
        next: (res: Result) => {
          switch (res.statusCode) {
            case "-1005":
              this.loaded = true;
              this.storage.remove(DeviceStorage.ItineraryList);
              break;

            case "2000":
              this.setItinerary(res);
              break;

            default:
              break;
          }
        },
        error: (error: Result) => {
          this.homeService
            .presentAlertError("DashBoard: Error Fetching Itineraries", error.message);
        },
        complete: () => {
          this.loaded = true;
        }
      });
  }


  async setItinerary(onlineData: any) {
    await this.storage.get(DeviceStorage.ItineraryList)
      .then((res: any) => {

        if (res) {

          let existingData: any = res.data.every((element: any) => {
            return element.CSL_Action_Needed_By === this.userData?.userId;
          });

          if (!existingData) {
            this.storage.remove(DeviceStorage.ItineraryList);
            this.storage.set(DeviceStorage.ItineraryList, onlineData);
          }

          return;
        }
        this.storage.set(DeviceStorage.ItineraryList, onlineData);
      })
      .finally(() => this.loadDashboardData());
  }


  async setNewItineraryData(offlineData: any, onlineData: any) {
    for (let i = 0; i < offlineData.data.length; i++) {
      const STORED_DATA = offlineData.data[i];

      for (let j = 0; j < onlineData.data.length; j++) {
        const INCOMING_DATA = onlineData.data[j];

        if (INCOMING_DATA.CSL_Ref_No === STORED_DATA.CSL_Ref_No) {
          this.setItineraryData(INCOMING_DATA, STORED_DATA);
        }
      }
    }

    this.storage.set(DeviceStorage.ItineraryList, onlineData);
  }


  setItineraryData(newData: any, element: any) {
    // * Started an Itnenary
    newData.Status = element.Status;
    newData.CSL_Longitude_Start = element.CSL_Longitude_Start;
    newData.CSL_Latitude_Start = element.CSL_Latitude_Start;
    newData.CSL_Start_Date = element.CSL_Start_Date;
    newData.CSL_Location_Time_Start = element.CSL_Location_Time_Start;
    newData.started_Sync_Status = element.started_Sync_Status;
    newData.ConfirmationDate = element.ConfirmationDate;

    // * Arrived an Itnenary
    newData.CSL_Longitude_Time_In = element.CSL_Longitude_Time_In;
    newData.CSL_Latitude_Time_In = element.CSL_Latitude_Time_In;
    newData.CSL_Time_In_Date = element.CSL_Time_In_Date;
    newData.CSL_Location_Time_In = element.CSL_Location_Time_In;
    newData.arrived_Sync_Status = element.arrived_Sync_Status;
    newData.ArrivalDate = element.ArrivalDate;

    // * Complete an Itnenary
    newData.CSL_Longitude_Time_Out = element.CSL_Longitude_Time_Out;
    newData.CSL_Latitude_Time_Out = element.CSL_Latitude_Time_Out;
    newData.CSL_Time_Out_Date = element.CSL_Time_Out_Date;
    newData.CSL_Location_Time_Out = element.CSL_Location_Time_Out;
    newData.CSL_Result = element.CSL_Result;
    newData.CSL_Result_Date = element.CSL_Result_Date;
    newData.CSL_Status_To_Remarks = element.CSL_Status_To_Remarks;
    newData.CSL_Result_Remarks = element.CSL_Result_Remarks;
    newData.completed_Sync_status = element.completed_Sync_status;
    newData.Attachment = (element.Attachment === undefined ? '' : element.Attachment);
    newData.Latitude = element.CSL_Latitude_Time_Out;
    newData.Longitude = element.CSL_Longitude_Time_Out;
    newData.Location = element.CSL_Location_Time_Out;
    newData.Distance = element.Distance;
    newData.Geofencing = element.Geofencing;
  }


  async loadDashboardData() {
    await this.storage.get(DeviceStorage.ItineraryList)
      .then((res: Result) => {
        this.resetVariables();
        this.total = res.data.length;
        this.pending = res.data.filter((x: any) => x?.Status === 'Pending').length;
        this.started = res.data.filter((x: any) => x?.Status === 'Started').length;
        this.arrived = res.data.filter((x: any) => x?.Status === 'Arrived').length;
        this.completed = res.data.filter((x: any) => x?.Status === 'Completed').length;
      }).finally(() => this.loaded = true);
  }


  doRefresh(event: any) {
    this.loaded = false;
    this.resetVariables();

    setTimeout(() => {
      this.getUserInfo();
      event.detail.complete();
    }, 3000);
  }


  showAnnouncementModal(isOpen: boolean) {
    this.announcementModal = isOpen;
  }


  resetVariables() {
    this.total = 0;
    this.arrived = 0;
    this.pending = 0;
    this.completed = 0;
    this.started = 0;
  }

  addClient() {
    this.homeService.addClient();
  }
}

