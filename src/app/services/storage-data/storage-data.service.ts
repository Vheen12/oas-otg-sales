import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { DeviceStorage } from 'src/global';

export interface Result {
  data?: [];
  message?: string;
  statusCode?: string;
}

@Injectable({
  providedIn: 'root'
})
export class StorageDataService {

  public storedProvinceList: any[] = [];

  constructor(
    private storage: Storage,
  ) { }


  public getProvinceList() {
    let param_data: any[] = [];

    this.storage.get(DeviceStorage.ProvinceList)
      .then((res: Result) => {
        if (res.statusCode !== "2000") {
          return;
        }

        res.data.forEach((element: any) => {
          param_data.push(element);
        })
      });

    return param_data;

  }


  public getCityList() {
    let param_data: any[] = [];

    this.storage.get(DeviceStorage.CityList)
      .then((res: Result) => {
        if (res.statusCode !== "2000") {
          return;
        }
        res.data.forEach((element: any) => {
          param_data.push(element);
        })
      });

    return param_data;

  }


  public getBarangayList() {
    let param_data: any[] = [];

    this.storage.get(DeviceStorage.BarangayList)
      .then((res: Result) => {
        if (res.statusCode !== "2000") {
          return;
        }
        res.data.forEach((element: any) => {
          param_data.push(element);
        })
      });

    return param_data;

  }


  public getActivityList() {
    let param_data: any[] = [];

    this.storage.get(DeviceStorage.ActivityList)
      .then((res: Result) => {
        if (res.statusCode !== "2000") {
          return;
        }
        res.data.forEach((element: any) => {
          param_data.push(element);
        })
      });

    return param_data;

  }


  public getClientSourceList() {
    let param_data: any[] = [];

    this.storage.get(DeviceStorage.ClientSourceList)
      .then((res: Result) => {
        if (res.statusCode !== "2000") {
          return;
        }
        res.data.forEach((element: any) => {
          param_data.push(element);
        })
      });

    return param_data;

  }


  public getOccupationList() {
    let param_data: any[] = [];

    this.storage.get(DeviceStorage.OccupationList)
      .then((res: Result) => {
        if (res.statusCode !== "2000") {
          return;
        }
        res.data.forEach((element: any) => {
          param_data.push(element);
        })
      });

    return param_data;
  }


  public getAgentList() {
    let param_data: any[] = [];

    this.storage.get(DeviceStorage.AgentList)
      .then((res: Result) => {
        if (res.statusCode !== "2000") {
          return;
        }
        res.data.forEach((element: any) => {
          param_data.push(element);
        })
      });

    return param_data;
  }


  public getCorporateList() {
    let param_data: any[] = [];

    this.storage.get(DeviceStorage.CorporateAccountList)
      .then((res: Result) => {
        if (res.statusCode !== "2000") {
          return;
        }
        res.data.forEach((element: any) => {
          param_data.push(element);
        })
      });

    return param_data;
  }


  public getProductList() {
    let param_data: any[] = [];

    this.storage.get(DeviceStorage.ProductList)
      .then((res: Result) => {
        if (res.statusCode !== "2000") {
          return;
        }
        res.data.forEach((element: any) => {
          param_data.push(element);
        })
      });

    return param_data;
  }


  public getModelList() {
    let param_data: any[] = [];

    this.storage.get(DeviceStorage.ProductModelList)
      .then((res: Result) => {
        if (res.statusCode !== "2000") {
          return;
        }
        res.data.forEach((element: any) => {
          param_data.push(element);
        })
      });

    return param_data;
  }

}
