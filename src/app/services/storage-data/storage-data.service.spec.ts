import { TestBed } from '@angular/core/testing';

import { StorageDataService } from './storage-data.service';
import { IonicStorageModule, Storage } from '@ionic/storage-angular';
IonicStorageModule
describe('StorageDataService', () => {
  let service: StorageDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[IonicStorageModule],
      providers:[Storage]
    });
    service = TestBed.inject(StorageDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
