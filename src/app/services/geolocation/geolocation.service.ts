import { Injectable } from '@angular/core';
import { HttpClient,  } from '@angular/common/http';
import { Geolocation } from '@capacitor/geolocation';
import { NativeGeocoder } from '@capgo/nativegeocoder';
import haversine from 'haversine-distance';

@Injectable({
  providedIn: 'root'
})
export class GeolocationService {
  coordinates: any;
  location: any;

  addressString: any;

  constructor(private _http: HttpClient) { }


  public checkLocationPermission = async () => {
    return (await Geolocation.checkPermissions()).location;
  }


  public requestLocationPermission = async () => {
    return (await Geolocation.requestPermissions()).location;
  }


  public async getCoordinates() {

    await Geolocation.getCurrentPosition().then((res: any) => {
      this.coordinates = '';
      this.coordinates = res;
    });

    return this.coordinates;

  }

  public async getLocation(currentlatitude: number, currentlongitude: number) {
    await NativeGeocoder.reverseGeocode({ latitude: currentlatitude, longitude: currentlongitude }).then((address) => {
      address.addresses.forEach(key => {
        let location: string = `${key.areasOfInterest[0]}, ${key.thoroughfare}, ${key.subLocality}, ${key.locality}, ${key.administrativeArea}, ${key.countryName}`;
        this.location = "";
        this.location = location;
      });
    }).catch(() => {
      this.location = 'Offline Location';
    });

    return this.location;
  }

  public async getAddressCoordinate(address: any) {
    await NativeGeocoder.forwardGeocode({ addressString: address })
      .then((addresses) => {
        this.addressString = "";
        this.addressString = addresses.addresses[0];
      }).catch(() => {
        this.addressString = undefined;
      });

    return this.addressString;
  }

  public async geofencing(firstLocation: any, secondLocation: any) {
    const distance = haversine(firstLocation, secondLocation);
    let status: boolean, geofence: string;

    if (distance < 10.5 && distance >= 1) {
      status = true;
      geofence = 'Within Area of Client';
    }
    else if (distance <= 20000 && distance >= 10.5) {
      status = false;
      geofence = 'Beyond Area of Client';
    }
    else if (distance > 20000) {
      status = false;
      geofence = 'Beyond Area of Assignment';
    }
    else {
      status = false;
      geofence = 'Beyond Area of Districting';
    }

    let data = {
      distance,
      status,
      geofence,
    }

    return data;
  }


  getAddressCoordinates(address: string) {
    return this._http.get(`https://geocode.maps.co/search?q=${address}`);
  }
}
