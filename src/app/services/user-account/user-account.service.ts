import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DeviceStorage, GlobalVariable } from 'src/global';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Browser } from '@capacitor/browser';
import { Storage } from '@ionic/storage';
import { NetworkConnectionService } from '../network-connection/network-connection.service';
@Injectable({
  providedIn: 'root'
})
export class UserAccountService {

  private baseApiUrl = GlobalVariable.BASE_API_URL;
  private CLIENTID = GlobalVariable.CLIENT_ID;
  private Appkey = GlobalVariable.APPKEY;

  constructor(
    private _http: HttpClient,
    private router: Router,
    private storage: Storage,
    private network: NetworkConnectionService,
  ) { }


  getAppVersion(token: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id: this.CLIENTID,
      Authorization: token

    });

    const data = {
      action: 'GET_APPVERSION',
      appKey: this.Appkey,
    };

    return this._http.post(this.baseApiUrl, data, { headers });

  }

  presentAlertError(header: string, message: string) {
    Swal.fire({
      title: header,
      text: message,
      icon: 'error',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Retry',
    });
  }

  presentNewUpdateAlert(newAppVersion: any) {
    Swal.fire({
      title: 'New Version!',
      text: 'Version ' + newAppVersion + ' is now available',
      icon: 'info',
      heightAuto: false,
      allowOutsideClick: false,
      showCancelButton: false,
      confirmButtonText: 'Download Now',
      confirmButtonColor: '#0e8140',
    }).then(async (result) => {
      if (result.isConfirmed) {
        await Browser.open({ url: 'https://adamis.adamco.com.ph/otg/apk/ADAMCO_OTG_' + newAppVersion + '.apk' });
      }
    });
  }

  presentStorageFullAlert() {
    Swal.fire({
      title: 'Warning!',
      text: 'Your Storage is almost Full! Please Connect to the internet to continue!',
      icon: 'warning',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Continue',
    });
  }

  showDeviceUpdateAlert() {
    Swal.fire({
      title: 'Application Update!',
      text: 'Your App is currently Updated!',
      icon: 'info',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Continue',
    });
  }

  async presentCheckUpdateLoader(message: string) {
    Swal.fire({
      title: 'Loading!',
      text: message,
      heightAuto: false,
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      }
    });
  }

  presentLogoutAlert() {
    Swal.fire({
      title: 'Log Out?',
      text: 'Are you sure you want to log out?',
      icon: 'question',
      heightAuto: false,
      allowOutsideClick: false,
      showCancelButton: true,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Log out',
    }).then((result) => {
      if (result.isConfirmed) {
        this.presentLogoutSuccess();
      }
    });
  }

  presentSyncAppAlert() {
    Swal.fire({
      title: 'Sync App Data?',
      text: 'Are you sure you want to Refresh App Data?',
      icon: 'question',
      heightAuto: false,
      allowOutsideClick: false,
      showCancelButton: true,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Sync',
    }).then((result) => {
      if (result.isConfirmed) {
        this.router.navigate(['/sync-mode']);
      }
    });
  }


  presentLogoutSuccess() {
    Swal.fire({
      title: 'Success!',
      text: 'Logout Successfully',
      icon: 'success',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Continue',
    }).then((result) => {
      if (result.isConfirmed) {
        this.storage.remove(DeviceStorage.UserInformation);
        localStorage.removeItem('Authenticated');
        localStorage.removeItem('announcement');

        this.router.navigate(['/login']);
      }
    });
  }

  addClient() {
    Swal.fire({
      title: 'Prospect',
      text: 'Do you want to Add a Prospect Client?',
      icon: 'question',
      heightAuto: false,
      allowOutsideClick: false,
      showCancelButton: true,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Add Client',
    }).then((result) => {
      if (result.isConfirmed) {

        this.network.checkNetworkConnection().then((res: boolean) => {
          res
            ? this.router.navigateByUrl("prospect-profile-form")
            : this.presentAlertError("You are offline!","Please connect to the internet to continue!");
        });

      }
    });
  }
}
