import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalVariable, DeviceStorage } from 'src/global';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { NetworkConnectionService } from '../network-connection/network-connection.service';
import Swal from 'sweetalert2';
@Injectable({
  providedIn: 'root'
})
export class HomeService {

  private baseApiUrl = GlobalVariable.BASE_API_URL;
  private CLIENTID = GlobalVariable.CLIENT_ID;
  private Appkey = GlobalVariable.APPKEY;

  constructor(
    private _http: HttpClient,
    private storage: Storage,
    private router: Router,
    private network: NetworkConnectionService,
  ) { }

  getAnnouncements(token: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id: this.CLIENTID,
      Authorization: token

    });

    const data = {
      action: 'GET_ANNOUNCEMENTS',
      appKey: this.Appkey,
    };

    return this._http.post(this.baseApiUrl, data, { headers });

  }


  getItinerary(emp_no: string, token: string) {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id: this.CLIENTID,
      Authorization: token

    });

    const data = {
      action: 'GET_ITINERARY',
      appKey: this.Appkey,
      agentID: emp_no
    };

    return this._http.post(this.baseApiUrl, data, { headers });

  }


  presentAlertError(header: string, message: string) {
    Swal.fire({
      title: header,
      text: message,
      icon: 'error',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Retry',
    });
  }

  presentTokenExpiredAlert() {
    Swal.fire({
      title: 'Info',
      text: 'Session End! Please Login again!',
      icon: 'info',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Continue',
    }).then((res) => {
      if (res.isConfirmed) {
        this.removeStorageData();
      }
    });
  }


  addClient() {
    Swal.fire({
      title: 'Prospect',
      text: 'Do you want to Add a Prospect Client?',
      icon: 'question',
      heightAuto: false,
      allowOutsideClick: false,
      showCancelButton: true,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Add Client',
    }).then((result) => {
      if (result.isConfirmed) {

        this.network.checkNetworkConnection().then((res: boolean) => {
          res
            ? this.router.navigateByUrl("prospect-profile-form")
            : this.presentAlertError("You are offline!","Please connect to the internet to continue!");
        });

      }
    });
  }


  removeStorageData() {
    this.storage.remove(DeviceStorage.UserInformation);
    this.storage.remove(DeviceStorage.ItineraryList);
    this.storage.remove(DeviceStorage.Itinerary_Coordinates);
    localStorage.removeItem('Authenticated');
    localStorage.removeItem('announcement');

    this.router.navigate(['/login']);
  }
}
