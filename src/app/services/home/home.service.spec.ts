import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { HomeService } from './home.service';
import { IonicStorageModule } from '@ionic/storage-angular';

describe('HomeService', () => {
  let service: HomeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        IonicStorageModule.forRoot(),
      ]
    });
    service = TestBed.inject(HomeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
