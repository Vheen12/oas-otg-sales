import { TestBed } from '@angular/core/testing';

import { TrListService } from './tr-list.service';

describe('TrListService', () => {
  let service: TrListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TrListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
