import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalVariable } from 'src/global';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TrListService {
  private baseApiUrl = GlobalVariable.BASE_API_URL;
  private CLIENTID = GlobalVariable.CLIENT_ID;
  private Appkey = GlobalVariable.APPKEY;

  constructor(
    private _http: HttpClient,
    private router: Router,
  ) { }


  getTransferReceiptList(token: string) {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id: this.CLIENTID,
      Authorization: token

    });

    const data = {
      action: 'GET_STOCK_TRANSFER_LIST',
      appKey: this.Appkey,
    };

    return this._http.post(this.baseApiUrl, data, { headers });

  }

  attachDeliveryReceipt(incomingdata: any, token: string) {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id: this.CLIENTID,
      Authorization: token

    });

    const data = {
      action: 'ATTACH_DELIVERY_RECEIPT_BAO',
      appKey: this.Appkey,
      data: incomingdata
    };

    return this._http.post(this.baseApiUrl, data, { headers });

  }


  async saveToAcumatica(transferNumber: any) {
    await fetch(`https://supplierportal.adamco.com.ph/AcumaticaEndpoint/Receive_Order/${transferNumber}`)
      .then((res: any) => {

        if (res.status !== 200) {
          this.presentAlertError("Oops!", "Error Saving to Acumatica!");;
        }

        console.log("Success");

        this.presentAlertSuccess("Successfully Submitted!");
      }).catch((error: Error) => {
        this.presentAlertError("Error Saving to Acumatica!", error.message);
      });
  }


  // TODO: Alerts

  async presentAlertSuccess(message: string) {
    Swal.fire({
      title: 'Success',
      text: message,
      icon: 'success',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Continue',
    }).then((res) => {
      if (res.isConfirmed) {
        this.router.navigateByUrl("tabs/tr-list");
      }
    });
  }

  presentAlertError(header: string, message: string) {
    Swal.fire({
      title: header,
      text: message,
      icon: 'error',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Retry',
    });
  }


}
