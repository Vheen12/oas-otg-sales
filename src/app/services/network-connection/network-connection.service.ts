import { Injectable } from '@angular/core';
import { Network } from '@capacitor/network';

@Injectable({
  providedIn: 'root'
})
export class NetworkConnectionService {

  constructor() { }

  public async checkNetworkConnection():Promise<boolean> {
    const status = await Network.getStatus();

    return status.connected;
  }
}
