import { TestBed } from '@angular/core/testing';

import { ProspectService } from './prospect.service';
import { HttpClientModule } from '@angular/common/http';
import { Storage } from '@ionic/storage-angular';

describe('ProspectService', () => {
  let service: ProspectService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ],
      providers: [Storage]
    });
    service = TestBed.inject(ProspectService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
