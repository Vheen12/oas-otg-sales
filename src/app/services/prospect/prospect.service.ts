import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DeviceStorage, GlobalVariable } from 'src/global';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Browser } from '@capacitor/browser';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class ProspectService {

  private baseApiUrl = GlobalVariable.BASE_API_URL;
  private CLIENTID = GlobalVariable.CLIENT_ID;
  private Appkey = GlobalVariable.APPKEY;

  constructor(
    private _http: HttpClient,
    private router: Router,
    private storage: Storage,
  ) { }

  insertOPIS_ClientInformation(jsonData: any, token: string) {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id: this.CLIENTID,
      Authorization: token

    });

    const data = {
      action: 'INSERT_PPF_SYNC',
      appKey: this.Appkey,
      data: jsonData
    };

    return this._http.post(this.baseApiUrl, data, { headers });

  }

  getNewProspectProfile(emp_no: string, token: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id: this.CLIENTID,
      Authorization: token

    });

    const data = {
      action: 'GET_NEW_PROSPECTPROFILE',
      appKey: this.Appkey,
      agentID: emp_no
    };

    return this._http.post(this.baseApiUrl, data, { headers });

  }



  async presentAlertSuccess(message: string) {
    Swal.fire({
      title: 'Success',
      text: message,
      icon: 'success',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Continue',
    });
  }

  async presentAlertWarning(message: string) {
    Swal.fire({
      title: '',
      text: message,
      icon: 'warning',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'OK',
    });
  }

  async presentAlertError(header: string, message: string) {
    Swal.fire({
      title: header,
      text: message,
      icon: 'error',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Retry',
    });
  }


  async presentClientVerification() {
    Swal.fire({
      title: 'Verifying!',
      text: "Checking Client Duplicate and Status...",
      heightAuto: false,
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      }
    });
  }
}
