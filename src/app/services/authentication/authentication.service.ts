import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalVariable } from 'src/global';
import Swal from 'sweetalert2';
import { Camera } from '@capacitor/camera';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private baseApiUrl = GlobalVariable.BASE_API_URL;
  private CLIENTID = GlobalVariable.CLIENT_ID;
  private Appkey = GlobalVariable.APPKEY;

  constructor(private _http: HttpClient) { }

  SignIn(loginData: any) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id: this.CLIENTID

    });

    const data = {
      action: 'LOGIN',
      appKey: this.Appkey,
      username: loginData.username,
      password: loginData.password,
      deviceID: loginData.deviceID,
      latitude: loginData.latitude,
      longitude: loginData.longitude,
      userLocation: loginData.userLocation,
      version: loginData.version,
    };

    return this._http.post(this.baseApiUrl, data, { headers });
  }


  checkCameraPermission() {
    Camera.checkPermissions().then((res: any) => {

      if (res?.camera === "granted" && res?.photos === "granted") {
        return;
      }

      this.requestCameraPermission();

    });
  }


  requestCameraPermission() {
    Camera.requestPermissions().then((res: any) => {

      console.log(res);

    }).catch((error: Error) => {
      this.presentAlertError("Request Camera Permission Error!", error?.message,);
    });
  }

  async presentAlertSuccess(message: string) {
    Swal.fire({
      title: 'Success',
      text: message,
      icon: 'success',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Continue',
    });
  }

  async presentAlertWarning(message: string) {
    Swal.fire({
      title: '',
      text: message,
      icon: 'warning',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'OK',
    });
  }

  async presentAlertError(header: string, message: string) {
    Swal.fire({
      title: header,
      text: message,
      icon: 'error',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Retry',
    });
  }

  syncOfflineDWA() {
    Swal.fire({
      title: 'Info',
      text: 'Sync Offline Itinerary?',
      icon: 'question',
      heightAuto: false,
      allowOutsideClick: false,
      showCancelButton: true,
      confirmButtonText: 'Sync Now',
      confirmButtonColor: '#0e8140',
    }).then((res) => {
      if (res.isConfirmed) {
        return true;
      }
    });
  }
  presentPendingAlert() {
    Swal.fire({
      title: 'Pending itinerary!',
      text: 'Please Sync Pending itinerary from offline storage to continue!',
      icon: 'warning',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Continue',
    });
  }


  async presentOfflineSyncAlert() {
    Swal.fire({
      title: 'Warning!',
      text: 'You are offline. Connect to the internet to sync data!',
      icon: 'warning',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Retry',
    });
  }
  async presentAlertSyncError() {
    Swal.fire({
      title: 'Oops!',
      text: 'Sync Failed!',
      icon: 'error',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Retry',
    });
  }
  async presentNoOfflineDataAlert() {
    Swal.fire({
      title: 'Oops!',
      text: 'There is no Offline Data Saved!',
      icon: 'error',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Continue',
    });
  }
  async showLoading() {
    Swal.fire({
      title: 'Syncing',
      text: 'Wait while offline data is being sync!',
      heightAuto: false,
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      }
    });
  }

  async showLogoutLoading() {
    Swal.fire({
      title: 'Logging Out',
      text: 'Wait for a few seconds!',
      heightAuto: false,
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      }
    });
  }

  async showLoginLoading() {
    Swal.fire({
      title: 'Logging In',
      text: 'Wait for a few seconds!',
      heightAuto: false,
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      }
    });
  }

  async showSurveyRedirecting() {
    Swal.fire({
      title: 'Redirecting to Survey Form',
      text: 'Wait for a few seconds!',
      heightAuto: false,
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      }
    });
  }
  async presentGeofencingAlert() {
    Swal.fire({
      title: 'Calculating',
      text: 'Checking Geofencing Status!',
      heightAuto: false,
      allowOutsideClick: false,
      showCancelButton: false,
      showConfirmButton: false,
      didOpen: () => {
        Swal.showLoading();
      }
    });
  }

  async closeAlert() {
    Swal.close();
  }
}
