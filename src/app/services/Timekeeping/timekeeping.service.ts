import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalVariable } from 'src/global';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { TextToSpeech } from '@capacitor-community/text-to-speech';

@Injectable({
  providedIn: 'root'
})
export class TimekeepingService {

  private baseApiUrl = GlobalVariable.BASE_API_URL;
  private CLIENTID = GlobalVariable.CLIENT_ID;
  private Appkey = GlobalVariable.APPKEY;


  constructor(
    private _http: HttpClient,
    private router: Router,
  ) { }

  getAttendanceData(emp_no: string, token: string) {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id: this.CLIENTID,
      Authorization: token

    });

    const data = {
      action: 'GET_ATTENDANCE',
      appKey: this.Appkey,
      agentID: emp_no
    };

    return this._http.post(this.baseApiUrl, data, { headers });

  }

  getAttendanceToday(emp_no: string, token: string) {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id: this.CLIENTID,
      Authorization: token

    });

    const data = {
      action: 'GET_ATTENDANCE_DATA',
      appKey: this.Appkey,
      agentID: emp_no
    };

    return this._http.post(this.baseApiUrl, data, { headers });

  }


  insertAttendance(jsonData: any, token: string) {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id: this.CLIENTID,
      Authorization: token

    });

    const data = {
      action: 'INSERT_ATTENDANCE',
      appKey: this.Appkey,
      accessType: 'Attendance',
      data: jsonData
    };

    return this._http.post(this.baseApiUrl, data, { headers });

  }



  presentAlertError(header: string, message: string) {
    Swal.fire({
      title: header,
      text: message,
      icon: 'error',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Retry',
    });
  }

  presentPendingAlert() {
    Swal.fire({
      title: 'Pending itinerary!',
      text: 'Please Sync Pending itinerary from offline storage to continue!',
      icon: 'warning',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Continue',
    });
  }

  async presentTextToSpeech(textMessage: string) {
    const speak = async () => {
      await TextToSpeech.speak({
        text: textMessage,
        lang: 'en-US',
        rate: 1.0,
        pitch: 1.0,
        volume: 1.0,
        category: 'ambient'
      }).catch((error) => {
        throw error;
      });
    };

    await speak();
  }


  async presentNoStartedItineraryAlert() {
    Swal.fire({
      title: 'No Itinerary Started!',
      text: 'There is no Itinerary Started. Please start an Itinerary first to time in!',
      icon: 'warning',
      heightAuto: false,
      allowOutsideClick: false,
      showCancelButton: true,
      confirmButtonText: "Go to DWA",
      confirmButtonColor: '#0e8140',
    }).then(async (result) => {
      if (result.isConfirmed) {
        await TextToSpeech.stop();
        this.router.navigateByUrl('/workschedule');
      }
    });
  }



  async presentPendingItineraryAlert() {
    Swal.fire({
      title: 'Pending Itineraries!',
      text: 'There are pending itineraries! Please Complete first to Time Out!',
      icon: 'warning',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Continue',
    }).then((result) => {
      if (result.isConfirmed) {
        TextToSpeech.stop();
      }
    });
  }

  async showSurveyRedirecting() {
    Swal.fire({
      title: 'Redirecting to Survey Form',
      text: 'Wait for a few seconds!',
      heightAuto: false,
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      }
    });
  }

  async closeAlert() {
    Swal.close();
  }

}
