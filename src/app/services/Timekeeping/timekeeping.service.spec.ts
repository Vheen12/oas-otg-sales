import { TestBed } from '@angular/core/testing';
import { TimekeepingService } from './timekeeping.service';
import { HttpClientModule } from '@angular/common/http';

describe('TimekeepingService', () => {
  let service: TimekeepingService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(TimekeepingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
