import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalVariable } from 'src/global';

@Injectable({
  providedIn: 'root'
})
export class SyncDataService {

  private baseApiUrl = GlobalVariable.BASE_API_URL;
  private CLIENTID = GlobalVariable.CLIENT_ID;
  private Appkey = GlobalVariable.APPKEY;

  constructor(private _http: HttpClient) { }

  getItinerary(emp_no: string,token: string){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id : this.CLIENTID,
      Authorization :  token

    });

    const data = {
      action:'GET_ITINERARY',
      appKey: this.Appkey,
      agentID: emp_no
    };

    return this._http.post(this.baseApiUrl, data,{headers});

  }

  getBranchList(token: string){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id : this.CLIENTID,
      Authorization :  token

    });

    const data = {
      action:'GET_BRANCH_LIST',
      appKey: this.Appkey,
    };

    return this._http.post(this.baseApiUrl, data,{headers});

  }

  getPromotions(emp_no: string,token: string){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id : this.CLIENTID,
      Authorization :  token

    });

    const data = {
      action:'GET_PROMOTIONS',
      appKey: this.Appkey,
      agentID: emp_no
    };

    return this._http.post(this.baseApiUrl, data,{headers});

  }

  getResultCode(emp_no: string,token: string){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id : this.CLIENTID,
      Authorization :  token

    });

    const data = {
      action:'GET_RESULT_CODE',
      appKey: this.Appkey,
      agentID: emp_no
    };

    return this._http.post(this.baseApiUrl, data,{headers});

  }

  getCorporateAccounts(emp_no: string,token: string){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id : this.CLIENTID,
      Authorization :  token

    });

    const data = {
      action:'GET_CORPORATE_ACCOUNTS',
      appKey: this.Appkey,
      agentID: emp_no
    };

    return this._http.post(this.baseApiUrl, data,{headers});

  }

  getAgents(emp_no: string,token: string){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id : this.CLIENTID,
      Authorization :  token

    });

    const data = {
      action:'GET_AGENTS',
      appKey: this.Appkey,
      agentID: emp_no
    };

    return this._http.post(this.baseApiUrl, data,{headers});

  }

  getASQList(emp_no: string,token: string){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id : this.CLIENTID,
      Authorization :  token

    });

    const data = {
      action:'GET_ASQ_LIST',
      appKey: this.Appkey,
      agentID: emp_no
    };

    return this._http.post(this.baseApiUrl, data,{headers});

  }

  getActivities(emp_no: string,token: string, userAccess: string){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id : this.CLIENTID,
      Authorization :  token

    });

    const data = {
      action:'GET_MKTG_ACTIVITY',
      appKey: this.Appkey,
      agentID: emp_no,
      module: userAccess
    };

    return this._http.post(this.baseApiUrl, data,{headers});

  }

  getProvince(emp_no: string,token: string){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id : this.CLIENTID,
      Authorization :  token

    });

    const data = {
      action:'GET_PROVINCE',
      appKey: this.Appkey,
      agentID: emp_no
    };

    return this._http.post(this.baseApiUrl, data,{headers});

  }

  getCity(emp_no: string,token: string){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id : this.CLIENTID,
      Authorization :  token

    });

    const data = {
      action:'GET_CITY',
      appKey: this.Appkey,
      agentID: emp_no
    };

    return this._http.post(this.baseApiUrl, data,{headers});

  }

  getBarangay(emp_no: string,token: string){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id : this.CLIENTID,
      Authorization :  token

    });

    const data = {
      action:'GET_BARANGAY',
      appKey: this.Appkey,
      agentID: emp_no
    };

    return this._http.post(this.baseApiUrl, data,{headers});

  }

  getOccupation(emp_no: string,token: string){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id : this.CLIENTID,
      Authorization :  token

    });

    const data = {
      action:'GET_OCCUPATION',
      appKey: this.Appkey,
      agentID: emp_no
    };

    return this._http.post(this.baseApiUrl, data,{headers});

  }

  getProduct(emp_no: string,token: string){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id : this.CLIENTID,
      Authorization :  token

    });

    const data = {
      action:'GET_PRODUCT_CLASS',
      appKey: this.Appkey,
      agentID: emp_no
    };

    return this._http.post(this.baseApiUrl, data,{headers});

  }

  getModel(emp_no: string,token: string){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id : this.CLIENTID,
      Authorization :  token

    });

    const data = {
      action:'GET_MODEL',
      appKey: this.Appkey,
      agentID: emp_no
    };

    return this._http.post(this.baseApiUrl, data,{headers});

  }

  getBrand(emp_no: string,token: string){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id : this.CLIENTID,
      Authorization :  token

    });

    const data = {
      action:'GET_BRAND',
      appKey: this.Appkey,
      agentID: emp_no
    };

    return this._http.post(this.baseApiUrl, data,{headers});

  }

  getAdvertisements(emp_no: string,token: string){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id : this.CLIENTID,
      Authorization :  token

    });

    const data = {
      action:'GET_ADVERTISEMENTS',
      appKey: this.Appkey,
      agentID: emp_no
    };

    return this._http.post(this.baseApiUrl, data,{headers});

  }


}
