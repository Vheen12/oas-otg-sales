import { TestBed } from '@angular/core/testing';

import { SyncDataService } from './sync-data.service';
import { HttpClientModule } from '@angular/common/http';

describe('SyncDataService', () => {
  let service: SyncDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ],
    });
    service = TestBed.inject(SyncDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
