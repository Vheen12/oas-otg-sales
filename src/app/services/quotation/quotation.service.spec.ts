import { TestBed } from '@angular/core/testing';
import { QuotationService } from './quotation.service';
import { HttpClientModule } from '@angular/common/http';

describe('QuotationService', () => {
  let service: QuotationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(QuotationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
