import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalVariable } from 'src/global';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class QuotationService {

  private baseApiUrl = GlobalVariable.BASE_API_URL;
  private CLIENTID = GlobalVariable.CLIENT_ID;
  private Appkey = GlobalVariable.APPKEY;

  constructor(private _http: HttpClient) { }

  attachSignature(incomingdata: any, token: string) {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id: this.CLIENTID,
      Authorization: token

    });

    const data = {
      action: 'ATTACH_SIGNATURE',
      appKey: this.Appkey,
      data: incomingdata
    };

    return this._http.post(this.baseApiUrl, data, { headers });

  }

  attachSignatureWithVideo(incomingdata: any, token: string) {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id: this.CLIENTID,
      Authorization: token

    });

    const data = {
      action: 'ATTACH_SIGNATURE_VIDEO',
      appKey: this.Appkey,
      data: incomingdata
    };

    return this._http.post(this.baseApiUrl, data, { headers });

  }


  async presentAlertSuccess(message: string) {
    Swal.fire({
      title: 'Success',
      text: message,
      icon: 'success',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Continue',
    });
  }

  async presentAlertWarning(message: string) {
    Swal.fire({
      title: '',
      text: message,
      icon: 'warning',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'OK',
    });
  }

  async presentAlertError(header: string, message: string) {
    Swal.fire({
      title: header,
      text: message,
      icon: 'error',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Retry',
    });
  }
}
