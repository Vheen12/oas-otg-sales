import { Injectable } from '@angular/core';
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera';
@Injectable({
  providedIn: 'root'
})
export class PhotoService {
  public photos: UserPhoto[] = [];

  constructor() { }

  public async addNewToGallery() {
    const capturedPhoto = await Camera.getPhoto({
      resultType: CameraResultType.Base64,
      saveToGallery: true,
      source: CameraSource.Camera,
      quality: 90,
    });

    // Save the picture and add it to photo collection
    const savedImageFile = await this.savePicture(capturedPhoto.base64String);
    this.photos.unshift(savedImageFile);

    return capturedPhoto.base64String;
  }


  public async savePicture(photo: any) {
    const fileName = new Date().getTime() + '.jpeg';

    return {
      filepath: fileName,
      webviewPath: photo
    };
  }
}

export interface UserPhoto {
  filepath: string;
  webviewPath: string | undefined;
}
