import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalVariable } from 'src/global';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class WorkscheduleService {

  private baseApiUrl = GlobalVariable.BASE_API_URL;
  private CLIENTID = GlobalVariable.CLIENT_ID;
  private Appkey = GlobalVariable.APPKEY;

  constructor(
    private _http: HttpClient,
    private router: Router,
  ) { }

  getItinerary(emp_no: string, token: string) {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id: this.CLIENTID,
      Authorization: token

    });

    const data = {
      action: 'GET_ITINERARY',
      appKey: this.Appkey,
      agentID: emp_no
    };

    return this._http.post(this.baseApiUrl, data, { headers });

  }


  startItnenary(dataCompress: any, token: string) {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id: this.CLIENTID,
      Authorization: token

    });

    const data = {
      action: 'UPDATE_ACCEPTANCE_STATUS',
      appKey: this.Appkey,
      data: dataCompress
    };

    return this._http.post(this.baseApiUrl, data, { headers });

  }

  arrivedItnenary(dataCompress: any, token: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id: this.CLIENTID,
      Authorization: token

    });

    const data = {
      action: 'UPDATE_ARRIVAL_STATUS',
      appKey: this.Appkey,
      data: dataCompress
    };

    return this._http.post(this.baseApiUrl, data, { headers });

  }



  getItineraryStatus(emp_no: string, token: string) {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id: this.CLIENTID,
      Authorization: token

    });

    const data = {
      action: 'GET_RESULT_CODE',
      appKey: this.Appkey,
      agentID: emp_no
    };

    return this._http.post(this.baseApiUrl, data, { headers });
  }

  updateItinerary(jsonData: any, token: string) {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id: this.CLIENTID,
      Authorization: token

    });

    const data = {
      action: 'UPDATE_ITINERARY',
      appKey: this.Appkey,
      data: jsonData
    };

    return this._http.post(this.baseApiUrl, data, { headers });

  }

  syncOfflineItinerary(jsonData: any, token: string) {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id: this.CLIENTID,
      Authorization: token

    });

    const data = {
      action: 'SYNC_ITENARY_ALL',
      appKey: this.Appkey,
      data: jsonData
    };

    return this._http.post(this.baseApiUrl, data, { headers });

  }

  checkRemovalPermission(userID: any, token: string) {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      client_id: this.CLIENTID,
      Authorization: token

    });

    const data = {
      action: 'CHECK_PERMISSION',
      appKey: this.Appkey,
      agentID: userID
    };

    return this._http.post(this.baseApiUrl, data, { headers });

  }



  // TODO: Alerts

  async presentAlertSuccess(message: string) {
    Swal.fire({
      title: 'Success',
      text: message,
      icon: 'success',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Continue',
    });
  }

  presentAlertError(header: string, message: string) {
    Swal.fire({
      title: header,
      text: message,
      icon: 'error',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Retry',
    });
  }

  presentEmptyAppData(header: string, message: string) {
    Swal.fire({
      title: header,
      text: message,
      icon: 'error',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Sync Now',
      showCancelButton: true,
    }).then((result) => {
      if (result.isConfirmed) {
        this.router.navigateByUrl("/sync-mode");
      }
    });
  }


  presentNoTimeInAlert() {
    Swal.fire({
      title: 'No Time In!',
      text: 'Please Time In First to start your itineraries!',
      icon: 'warning',
      heightAuto: false,
      allowOutsideClick: false,
      showCancelButton: true,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Time In',
    }).then((result) => {
      if (result.isConfirmed) {
        this.router.navigateByUrl("timekeeping");
      }
    });
  }

  presentOfflineSyncAlert() {
    Swal.fire({
      title: 'Warning!',
      text: 'You are offline. Connect to the internet to sync data!',
      icon: 'warning',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Retry',
    });
  }

  presentOfflineCheckPermissionAlert() {
    Swal.fire({
      title: 'Warning!',
      text: 'You are offline. Connect to the internet to continue!',
      icon: 'warning',
      heightAuto: false,
      allowOutsideClick: false,
      confirmButtonColor: '#0e8140',
      confirmButtonText: 'Retry',
    });
  }


  showLoading() {
    Swal.fire({
      title: 'Syncing',
      text: 'Wait while offline data is being sync!',
      heightAuto: false,
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      }
    });
  }

  showActionLoading() {
    Swal.fire({
      title: 'Saving!',
      text: 'Wait for a few seconds',
      heightAuto: false,
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      }
    });
  }
}




