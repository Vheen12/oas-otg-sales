import { TestBed } from '@angular/core/testing';

import { WorkscheduleService } from './workschedule.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('WorkscheduleService', () => {
  let service: WorkscheduleService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(WorkscheduleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
